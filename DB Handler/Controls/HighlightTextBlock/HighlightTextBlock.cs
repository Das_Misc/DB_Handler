﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Extra.Controls
{
    public class HighlightTextBlock : TextBlock
    {
        #region Properties

        public static readonly DependencyProperty HighlightBrushProperty =
            DependencyProperty.Register("HighlightBrush", typeof(Brush),
            typeof(HighlightTextBlock), new FrameworkPropertyMetadata(Brushes.Yellow, FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(UpdateHighlighting)));

        public static readonly DependencyProperty HighlightPhraseProperty =
            DependencyProperty.Register("HighlightPhrase", typeof(string),
            typeof(HighlightTextBlock), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(UpdateHighlighting)));

        public static readonly DependencyProperty IsCaseSensitiveProperty =
            DependencyProperty.Register("IsCaseSensitive", typeof(bool),
            typeof(HighlightTextBlock), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(UpdateHighlighting)));

        public new static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string),
            typeof(HighlightTextBlock), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(UpdateHighlighting)));

        public Brush HighlightBrush
        {
            get { return (Brush)GetValue(HighlightBrushProperty); }
            set { SetValue(HighlightBrushProperty, value); }
        }

        public string HighlightPhrase
        {
            get { return (string)GetValue(HighlightPhraseProperty); }
            set { SetValue(HighlightPhraseProperty, value); }
        }

        public bool IsCaseSensitive
        {
            get { return (bool)GetValue(IsCaseSensitiveProperty); }
            set { SetValue(IsCaseSensitiveProperty, value); }
        }

        public new string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        private static void UpdateHighlighting(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ApplyHighlight(d as HighlightTextBlock);
        }

        #endregion Properties

        #region Members

        /// <summary>
        ///     Render the text to the screen and apply highlighting where appropriate
        /// </summary>
        private static void ApplyHighlight(HighlightTextBlock tb)
        {
            tb.Inlines.Clear();

            //Not looking for anything, just add all the text like a normal text block
            if (string.IsNullOrEmpty(tb.HighlightPhrase))
                tb.Inlines.Add(tb.Text);
            else
            {
                var indices = IndexOfAll(tb.Text, tb.HighlightPhrase, tb.IsCaseSensitive);

                //if highlightPhrase doesn't exist in text
                if (indices.Count == 0)
                    tb.Inlines.Add(tb.Text);

                int written = 0;

                foreach (var index in indices)
                {
                    //Add up until the index
                    tb.Inlines.Add(tb.Text.Substring(written, index - written));

                    //Add the found as highlighted
                    tb.Inlines.Add(new Run(tb.Text.Substring(index, tb.HighlightPhrase.Length))
                    {
                        Background = tb.HighlightBrush
                    });

                    //Keep a marker of what we've written out.
                    written = index + tb.HighlightPhrase.Length;

                    //We don't have anything else to highlight
                    if (indices.TrueForAll(x => written > x))
                    {
                        //Add the rest
                        tb.Inlines.Add(tb.Text.Substring(written));

                        break;
                    }
                }
            }
        }

        /// <summary>
        ///     Get a list of all the locations in a string that the text is found.
        /// </summary>
        /// <param name="sourceString">The text we are looking in</param>
        /// <param name="searchText">The text that will be found in <seealso cref="sourceString"/> </param>
        /// <param name="isCaseSensitive">Search on case sensitivity</param>
        /// <returns></returns>
        private static List<int> IndexOfAll(string sourceString, string searchText, bool isCaseSensitive)
        {
            RegexOptions options = isCaseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase;

            return Regex.Matches(sourceString, Regex.Escape(searchText), options).Cast<Match>().Select(m => m.Index).ToList();
        }

        #endregion Members
    }
}
