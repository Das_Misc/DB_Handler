﻿using System;

namespace Extra.Models
{
    public class DownloadStatus : EventArgs
    {
        internal DownloadStatus(long bytesReceived, long totalBytesToReceive, string status, string filename)
        {
            LosslessProgress = (double)(bytesReceived * 100) / totalBytesToReceive;

            LossyProgress = (int)LosslessProgress;

            BytesReceived = bytesReceived;

            TotalBytesToReceive = totalBytesToReceive;

            Filename = filename;

            Status = status;
        }

        internal DownloadStatus(string status, string filename)
        {
            if (status == DB_Handler.Properties.Resources.Canceled)
            {
                LosslessProgress = 0;
                LossyProgress = 0;
            }
            else if (status == DB_Handler.Properties.Resources.Downloaded)
            {
                LosslessProgress = 100;
                LossyProgress = 100;
            }

            Filename = filename;
            Status = status;
        }

        public long BytesReceived { get; set; }
        public string Filename { get; set; }
        public double LosslessProgress { get; set; }

        public int LossyProgress { get; set; }
        public string Status { get; set; }
        public long TotalBytesToReceive { get; set; }
    }
}
