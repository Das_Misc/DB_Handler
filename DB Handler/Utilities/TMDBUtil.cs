﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Extra.Utilities
{
    internal class TMDBUtil
    {
        private static readonly byte[] tmdbKey = { 0xF5, 0xDE, 0x66, 0xD2, 0x68, 0x0E, 0x25, 0x5B, 0x2D, 0xF7, 0x9E, 0x74, 0xF8, 0x90, 0xEB, 0xF3, 0x49, 0x26, 0x2F, 0x61,
            0x8B, 0xCA, 0xE2, 0xA9, 0xAC, 0xCD, 0xEE, 0x51, 0x56, 0xCE, 0x8D, 0xF2, 0xCD, 0xF2, 0xD4, 0x8C, 0x71, 0x17, 0x3C, 0xDC, 0x25, 0x94, 0x46, 0x5B,
            0x87, 0x40, 0x5D, 0x19, 0x7C, 0xF1, 0xAE, 0xD3, 0xB7, 0xE9, 0x67, 0x1E, 0xEB, 0x56, 0xCA, 0x67, 0x53, 0xC2, 0xE6, 0xB0 };

        internal static string GetTitle(string titleID)
        {
            if (!Regex.IsMatch(titleID, @"[A-Z]{4}\d{5}"))
                return null;

            byte[] seed = Encoding.UTF8.GetBytes(titleID + "_00");

            string hash = Convert(seed);

            string url = string.Format("http://tmdb.np.dl.playstation.net/tmdb/{0}_00_{1}/{0}_00.xml", titleID, hash);

            return FetchXMLName(url);
        }

        internal static bool? GetTitleExists(string titleID)
        {
            if (!Regex.IsMatch(titleID, @"[A-Z]{4}\d{5}"))
                return null;

            byte[] seed = Encoding.UTF8.GetBytes(titleID + "_00");

            string hash = Convert(seed);

            string url = string.Format("http://tmdb.np.dl.playstation.net/tmdb/{0}_00_{1}/{0}_00.xml", titleID, hash);

            return FetchXML(url);
        }

        private static string Convert(byte[] seed)
        {
            var hmac = new HMAC();
            hmac.DoInit(tmdbKey);
            hmac.DoUpdate(seed, 0, seed.Length);

            byte[] hash = hmac.DoFinalButGetHash();

            return BitConverter.ToString(hash).Replace("-", "");
        }

        private static bool? FetchXML(string url)
        {
            StringBuilder sb = new StringBuilder();

            var xml = new XmlDocument();
            try
            {
                xml.Load(url);
            }
            catch (WebException)
            {
                return null;
            }
            catch (XmlException)
            {
                return null;
            }

            return xml.DocumentElement.HasAttributes || (xml.DocumentElement.ChildNodes.Count > 0);
        }

        private static string FetchXMLName(string url)
        {
            StringBuilder sb = new StringBuilder();

            var xml = new XmlDocument();
            try
            {
                xml.Load(url);
            }
            catch (WebException)
            {
                return null;
            }
            catch (XmlException)
            {
                return null;
            }
            
            foreach (XmlNode node in xml.DocumentElement.ChildNodes)
                if (node.Name == "name")
                    return node.InnerText;
            
            return null;
        }
    }
}
