﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using DB_Handler.Models;
using DB_Handler.Properties;
using DB_Handler.Views;
using Extra.Controls;
using Extra.Functions;
using Extra.Utilities;

namespace DB_Handler.Functions
{
    internal class DatabaseFunctions
    {
        #region Fields

        private const string LidRegex = @"^[A-Z\d_]{16}$";
        private const string LinkRegex = @"http://.*?\.pkg|http://.*?\.p3t|http://.*?\.mp4|http://.*?\.json";
        private const string RapRegex = @"[A-Fa-f\d]{32}";
        private const string UpdateLinkRegex = @"b0.ww.np.|gs.ww.np.|gs2/ppkgo/prod";

        private static readonly string[] ConsolesToSearch = {"ps3", "ps4", "psp", "psv"};

        #endregion Fields

        #region Importing/Exporting methods

        internal static void ExportDb(string fileToExportTo)
        {
            using (var dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            {
                dbConnection.Open();

                var contentsList = new List<string>();

                foreach (var console in ConsolesToSearch)
                    using (var command = dbConnection.CreateCommand())
                    {
                        command.CommandText = $"SELECT * FROM {console}";

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                            contentsList.Add(
                                $"\"{reader["cid"]}\";\"{reader["rap"]}\";\"{Regex.Replace(reader["title"].ToString(), @"[;""!®™]*", "")}\";\"{reader["size"]}\";\"{reader["link"]}\"");
                    }

                File.WriteAllLines(fileToExportTo, contentsList);
            }
        }

        internal static void ExportTitles(string fileToExportTo)
        {
            using (var dbConnection =
                new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
            {
                dbConnection.Open();

                var contentsList = new List<string>();

                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM titles";

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                        contentsList.Add(string.Format("{0} {1}", reader["titleid"],
                            Regex.Replace(reader["title"].ToString(), @"[;""!®™]*", "")));
                }

                File.WriteAllLines(fileToExportTo, contentsList);
            }
        }

        internal static async void Import(MainWindow mw, string fileToImport, IProgress<double> progress)
        {
            await Task.Run(() =>
            {
                if (fileToImport.EndsWith(".tsv", StringComparison.InvariantCultureIgnoreCase))
                    ImportTsv(mw, fileToImport, progress);

                if (fileToImport.EndsWith(".csv", StringComparison.InvariantCultureIgnoreCase))
                    ImportCsv(mw, fileToImport, progress);

                if (fileToImport.EndsWith(".db", StringComparison.InvariantCultureIgnoreCase))
                    ImportSqlDb(mw, fileToImport, progress);

                if (fileToImport.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase) ||
                    !File.Exists(fileToImport))
                    ImportTxt(mw, fileToImport, progress);

                DeleteDuplicateEntries(mw);

                RefreshDatabaseEntries(mw);

                MainWindow.AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"),
                    Resources.ImportedContents));
            });
        }

        internal static void ImportTitles(string[] titlesFileContents)
        {
            using (var dbConnection =
                new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
            {
                dbConnection.Open();

                using (var transaction = dbConnection.BeginTransaction())
                {
                    using (var dropTitlesCommand = dbConnection.CreateCommand())
                    {
                        dropTitlesCommand.CommandText = "DROP TABLE titles";
                        dropTitlesCommand.ExecuteNonQuery();
                    }

                    using (var createTitlesCommand = dbConnection.CreateCommand())
                    {
                        createTitlesCommand.CommandText =
                            "CREATE TABLE titles(titleid TEXT(9) PRIMARY KEY, title TEXT, UNIQUE(titleid)) WITHOUT ROWID";
                        createTitlesCommand.ExecuteNonQuery();
                    }

                    for (int i = 0; i < titlesFileContents.Length; i++)
                    {
                        string[] row = titlesFileContents[i].Split(new[] {' '}, 2);

                        string titleId = row[0];
                        string title = row[1];

                        using (var insertTitlesCommand = dbConnection.CreateCommand())
                        {
                            insertTitlesCommand.CommandText =
                                "INSERT OR IGNORE INTO titles(titleid, title) VALUES (@titleID, @title)";

                            insertTitlesCommand.Parameters.Add(new SQLiteParameter("@titleID", titleId));
                            insertTitlesCommand.Parameters.Add(new SQLiteParameter("@title", title));

                            insertTitlesCommand.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                }
            }
        }

        private static void AddContentsToDb(SQLiteConnection dbConnection, ref SQLiteTransaction transaction,
            string cid, string rap, string title, long size, string link)
        {
            string titleId = cid.Substring(7, 9);

            string console = GetConsole(titleId);

            using (var insertEntryCommand = dbConnection.CreateCommand())
            {
                if (!console.Any(char.IsLetterOrDigit))
                    throw new Exception("SQL injection");

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                insertEntryCommand.CommandText = "INSERT INTO " + console +
                                                 "(cid, rap, title, size, link) VALUES (@cid, @rap, @title, @size, @link)";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                insertEntryCommand.Parameters.Add(new SQLiteParameter("@cid", cid));
                insertEntryCommand.Parameters.Add(new SQLiteParameter("@rap", rap));
                insertEntryCommand.Parameters.Add(new SQLiteParameter("@title", title));
                insertEntryCommand.Parameters.Add(new SQLiteParameter("@size", size));
                insertEntryCommand.Parameters.Add(new SQLiteParameter("@link", link));

                Retry.Do(insertEntryCommand.ExecuteNonQuery, 5);
            }

            int titleCount;

            using (var titleCheckCommand = dbConnection.CreateCommand())
            {
                titleCheckCommand.CommandText = "SELECT COUNT(*) FROM titles WHERE titleid = @titleID";

                titleCheckCommand.Parameters.Add(new SQLiteParameter("@titleID", titleId));

                titleCount = Convert.ToInt32(titleCheckCommand.ExecuteScalar());
            }

            if (titleCount == 0)
            {
                using (var insertTitleCommand = dbConnection.CreateCommand())
                {
                    insertTitleCommand.CommandText = "INSERT INTO titles(titleid, title) VALUES (@titleID, @title)";

                    insertTitleCommand.Parameters.Add(new SQLiteParameter("@titleID", titleId));
                    insertTitleCommand.Parameters.Add(new SQLiteParameter("@title", title));

                    Retry.Do(insertTitleCommand.ExecuteNonQuery, 5);
                }

                transaction.Commit();

                transaction = dbConnection.BeginTransaction();
            }
        }

        private static void AddMoviesToDb(SQLiteConnection dbConnection, string titleId, string title, string link)
        {
            using (var insertMovieCommand = dbConnection.CreateCommand())
            {
                insertMovieCommand.CommandText =
                    "INSERT OR IGNORE INTO videos(titleid, title, link) VALUES (@titleID, @title, @link)";

                insertMovieCommand.Parameters.Add(new SQLiteParameter("@titleID", titleId));
                insertMovieCommand.Parameters.Add(new SQLiteParameter("@title", title));
                insertMovieCommand.Parameters.Add(new SQLiteParameter("@link", link));

                insertMovieCommand.ExecuteNonQuery();
            }
        }

        private static void AddTitlesToDb(SQLiteConnection dbConnection, string titleId, string title)
        {
            using (var insertTitleCommand = dbConnection.CreateCommand())
            {
                insertTitleCommand.CommandText =
                    "INSERT OR IGNORE INTO titles(titleid, title) VALUES (@titleID, @title)";

                insertTitleCommand.Parameters.Add(new SQLiteParameter("@titleID", titleId));
                insertTitleCommand.Parameters.Add(new SQLiteParameter("@title", title));

                insertTitleCommand.ExecuteNonQuery();
            }
        }

        private static string GetConsole(string titleId)
        {
            if (titleId.StartsWith("NPNA", StringComparison.InvariantCultureIgnoreCase))
                return "psv";

            switch (titleId[0])
            {
                case 'B':
                case 'N':
                    return "ps3";

                case 'C':
                    return "ps4";

                case 'P':
                    return "psv";

                case 'U':
                    return "psp";

                default:
                    return "Unknown console";
            }
        }

        private static void ImportContentsToDb(MainWindow mw, SQLiteConnection dbConnection,
            ref SQLiteTransaction transaction, string link, string rap = "")
        {
            if (!link.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                return;

            if (Regex.IsMatch(link, UpdateLinkRegex))
                return;

            link = link.Replace("apollo.dl.", "zeus.dl.");
            link = link.Replace("ares.dl.", "zeus.dl.");
            link = link.Replace("nsx.np.dl.", "zeus.dl.");
            link = link.Replace("poseidon.dl.", "zeus.dl.");
            if (link.Contains("?"))
                link = link.Remove(link.IndexOf("?", StringComparison.Ordinal));

            if (!Regex.IsMatch(rap.ToUpper(), RapRegex))
                rap = string.Empty;

            if (link.EndsWith(".json", StringComparison.InvariantCultureIgnoreCase))
            {
                string[] linkCollection = PSWebStore.GetJSONInfo(link);

                if (linkCollection != null)
                    foreach (var jsonLink in linkCollection)
                        ImportContentsToDb(mw, dbConnection, ref transaction, jsonLink, rap);

                return;
            }

            if (link.EndsWith(".xml", StringComparison.InvariantCultureIgnoreCase))
            {
                string[] links = PSWebStore.GetXMLInfo(link);

                if (links != null)
                    foreach (var partLink in links)
                        ImportContentsToDb(mw, dbConnection, ref transaction, partLink, rap);

                return;
            }

            if (link.EndsWith(".p3t", StringComparison.InvariantCultureIgnoreCase))
            {
                P3TInfo p3tInfo = PSWebStore.GetP3TInfo(link);
                AddContentsToDb(dbConnection, ref transaction, p3tInfo.Cid, string.Empty, p3tInfo.Title, p3tInfo.Size,
                    link);
                RefreshDatabaseEntriesRealTime(mw, p3tInfo.Cid, string.Empty, p3tInfo.Title, p3tInfo.Size, link);

                return;
            }

            PKGInfo pkgInfo = PSWebStore.GetPKGInfo(link);
            if (pkgInfo == null)
            {
                link = link.Replace("zeus.dl.", "apollo.dl.");
                pkgInfo = PSWebStore.GetPKGInfo(link);
            }

            if (string.IsNullOrEmpty(pkgInfo.TitleID))
            {
                if (!string.IsNullOrWhiteSpace(rap) && pkgInfo.LicenseType < 3 && pkgInfo.IsPS3orPSPPKG)
                {
                    bool? isValidRap =
                        RAPFunctions.GetIsValidRAPFromLink(link, rap.ToUpper(), App.DBHSettings.RAPsPath);

                    if (isValidRap.HasValue && isValidRap.Value)
                        MainWindow.AppLog.Add(
                            $"[{DateTime.Now.TimeOfDay:hh\\:mm\\:ss}] {string.Format(Resources.IsValidRAPValid, rap.ToUpper(), pkgInfo.CID)}");
                    else if (isValidRap.HasValue)
                    {
                        MainWindow.AppLog.Add(
                            $"[{DateTime.Now.TimeOfDay:hh\\:mm\\:ss}] {string.Format(Resources.IsValidRAPInvalid, rap.ToUpper(), pkgInfo.CID)}");

                        rap = string.Empty;
                    }
                    else
                        MainWindow.AppLog.Add(
                            $"[{DateTime.Now.TimeOfDay:hh\\:mm\\:ss}] {string.Format(Resources.IsValidRAPUnchecked, rap.ToUpper(), pkgInfo.CID)}");
                }
                else if (string.IsNullOrWhiteSpace(rap) && pkgInfo.LicenseType < 3 && pkgInfo.IsPS3orPSPPKG)
                    MainWindow.AppLog.Add(
                        $"[{DateTime.Now.TimeOfDay:hh\\:mm\\:ss}] {string.Format(Resources.MissingRAP, pkgInfo.CID)}");
                else if (!string.IsNullOrWhiteSpace(rap) && pkgInfo.LicenseType == 3 && pkgInfo.IsPS3orPSPPKG)
                {
                    MainWindow.AppLog.Add(
                        $"[{DateTime.Now.TimeOfDay:hh\\:mm\\:ss}] {string.Format(Resources.FreeNoRAP, rap.ToUpper(), pkgInfo.CID)}");

                    rap = string.Empty;
                }

                AddContentsToDb(dbConnection, ref transaction, pkgInfo.CID, rap.ToUpper(), pkgInfo.Title, pkgInfo.Size,
                    link);

                RefreshDatabaseEntriesRealTime(mw, pkgInfo.CID, rap.ToUpper(), pkgInfo.Title, pkgInfo.Size, link);
            }
            else
                AddMoviesToDb(dbConnection, pkgInfo.TitleID, pkgInfo.Title, link);
        }

        private static void ImportTsv(MainWindow mw, string tsvFile, IProgress<double> progress)
        {
            byte[] tsvContents = File.ReadAllBytes(tsvFile);

            double total = File.ReadAllLines(tsvFile).Length;

            CreateTables();

            using (SQLiteConnection dbConnection =
                new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
            using (MemoryStream ms = new MemoryStream(tsvContents))
            using (StreamReader reader = new StreamReader(ms, Encoding.UTF8))
            {
                dbConnection.Open();

                var transaction = dbConnection.BeginTransaction();

                string[] headers = reader.ReadLine().ToLower().Split('\t');

                int linkHeader = 3;
                int rapHeader = 4;
                for (int i = 0; i < headers.Length; i++)
                {
                    if (headers[i] == "pkg direct link")
                        linkHeader = i;

                    if (headers[i] == "rap" || headers[i] == "zrif")
                        rapHeader = i;
                }

                double current = 0;
                int count = 0;

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    string[] row = line.Split('\t');

                    for (int i = 0; i < row.Length; i++)
                        row[i] = row[i].Trim('"');

                    string link = row[linkHeader];
                    string rap = row[rapHeader];

                    ImportContentsToDb(mw, dbConnection, ref transaction, link, rap);

                    current++;
                    count++;

                    progress.Report(current * 100 / total);

                    if (count >= 1000)
                    {
                        transaction.Commit();
                        transaction.Dispose();
                        count = 0;
                        transaction = dbConnection.BeginTransaction();
                    }
                }

                if (count > 0)
                {
                    transaction.Commit();
                    transaction.Dispose();
                }
            }
        }

        private static void ImportCsv(MainWindow mw, string csvFile, IProgress<double> progress)
        {
            byte[] csvContents = File.ReadAllBytes(csvFile);

            double total = File.ReadAllLines(csvFile).Length;

            CreateTables();

            using (SQLiteConnection dbConnection =
                new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
            using (MemoryStream ms = new MemoryStream(csvContents))
            using (StreamReader reader = new StreamReader(ms, Encoding.UTF8))
            {
                dbConnection.Open();

                var transaction = dbConnection.BeginTransaction();

                double current = 0;
                int count = 0;

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine().Replace(" ", "");

                    string[] row = line.Split(new[] {"\";\""}, StringSplitOptions.None);

                    if (row.Length == 1)
                        row = line.Split(new[] {"\",\""}, StringSplitOptions.None);

                    if (row.Length == 1)
                        row = line.Split(new[] {";"}, StringSplitOptions.None);

                    for (int i = 0; i < row.Length; i++)
                        row[i] = row[i].Trim('"');

                    string link = row[4];
                    string rap = row[6];

                    ImportContentsToDb(mw, dbConnection, ref transaction, link, rap);

                    current++;
                    count++;

                    progress.Report(current * 100 / total);

                    if (count >= 1000)
                    {
                        transaction.Commit();
                        transaction.Dispose();
                        count = 0;
                        transaction = dbConnection.BeginTransaction();
                    }
                }

                if (count > 0)
                {
                    transaction.Commit();
                    transaction.Dispose();
                }
            }
        }

        private static void ImportSqlDb(MainWindow mw, string dbFile, IProgress<double> progress)
        {
            double total = 0;

            CreateTables();

            using (SQLiteConnection dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            using (SQLiteConnection importDbConnection =
                new SQLiteConnection($@"Data Source = {dbFile}; Version = 3"))
            {
                dbConnection.Open();
                importDbConnection.Open();

                var transaction = dbConnection.BeginTransaction();

                foreach (var console in ConsolesToSearch)
                    using (var consoleCheckCommand = importDbConnection.CreateCommand())
                    {
#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                        consoleCheckCommand.CommandText = $"SELECT COUNT(*) FROM {console}";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                        total += Convert.ToInt32(consoleCheckCommand.ExecuteScalar());
                    }

                using (var videosCheckCommand = importDbConnection.CreateCommand())
                {
                    videosCheckCommand.CommandText = "SELECT COUNT(*) FROM videos";

                    total += Convert.ToInt32(videosCheckCommand.ExecuteScalar());
                }

                using (var titlesCheckCommand = importDbConnection.CreateCommand())
                {
                    titlesCheckCommand.CommandText = "SELECT COUNT(*) FROM titles";

                    total += Convert.ToInt32(titlesCheckCommand.ExecuteScalar());
                }

                double current = 0;

                foreach (var console in ConsolesToSearch)
                    using (var consolesCommand = importDbConnection.CreateCommand())
                    {
#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                        consolesCommand.CommandText = $"SELECT * FROM {console}";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                        var reader = consolesCommand.ExecuteReader();

                        while (reader.Read())
                        {
                            string cid = reader["cid"].ToString();
                            string link = reader["link"].ToString();
                            string rap = reader["rap"].ToString();
                            long size = (long) reader["size"];
                            string title = reader["title"].ToString();

                            AddContentsToDb(dbConnection, ref transaction, cid, rap, title, size, link);

                            RefreshDatabaseEntriesRealTime(mw, cid, rap, title, size, link);

                            current++;

                            progress.Report(current * 100 / total);
                        }
                    }

                using (var videosCommand = importDbConnection.CreateCommand())
                {
                    videosCommand.CommandText = "SELECT * FROM videos";

                    var reader = videosCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        string titleId = reader["titleid"].ToString();
                        string title = reader["title"].ToString();
                        string link = reader["link"].ToString();

                        AddMoviesToDb(dbConnection, titleId, title, link);

                        current++;

                        progress.Report((current * 100) / total);
                    }
                }

                using (var titlesCommand = importDbConnection.CreateCommand())
                {
                    titlesCommand.CommandText = "SELECT * FROM titles";

                    var reader = titlesCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        string titleId = reader["titleid"].ToString();
                        string title = reader["title"].ToString();

                        AddTitlesToDb(dbConnection, titleId, title);

                        current++;

                        progress.Report((current * 100) / total);
                    }
                }

                transaction.Commit();

                transaction.Dispose();
            }
        }

        private static void ImportTxt(MainWindow mw, string txtFile, IProgress<double> progress)
        {
            byte[] txtContents;

            double total;

            if (File.Exists(txtFile))
            {
                txtContents = File.ReadAllBytes(txtFile);

                total = File.ReadAllLines(txtFile).Select(x => x)
                    .Where(x => Regex.IsMatch(x, LinkRegex, RegexOptions.RightToLeft))
                    .ToArray()
                    .Length;
            }
            else
            {
                txtContents = Encoding.UTF8.GetBytes(txtFile);

                string[] lines = txtFile.Split('\n');

                total = lines.Select(x => x)
                    .Where(x => Regex.IsMatch(x, LinkRegex, RegexOptions.RightToLeft))
                    .ToArray()
                    .Length;
            }

            CreateTables();

            using (var dbConnection = new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            using (var ms = new MemoryStream(txtContents))
            using (var reader = new StreamReader(ms, Encoding.UTF8))
            {
                dbConnection.Open();

                var transaction = dbConnection.BeginTransaction();

                string link = string.Empty;

                double current = 0;
                int count = 0;

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine().Replace(" ", "");

                    if (Regex.IsMatch(line, LinkRegex, RegexOptions.RightToLeft))
                    {
                        if (string.IsNullOrEmpty(link))
                            link = Regex.Match(line, LinkRegex, RegexOptions.RightToLeft).Value;
                        else
                        {
                            // A new link has been found although we already had one in-memory, but no RAP value. Let's add the one in memory first.
                            ImportContentsToDb(mw, dbConnection, ref transaction, link);

                            current++;
                            count++;

                            progress.Report((current * 100) / total);

                            link = Regex.Match(line, LinkRegex, RegexOptions.RightToLeft).Value;
                        }

                        continue;
                    }

                    if (Regex.IsMatch(line.Replace(" ", ""), RapRegex) && !string.IsNullOrEmpty(link))
                    {
                        string rap = Regex.Match(line.Replace(" ", ""), RapRegex).Value.ToUpper();

                        ImportContentsToDb(mw, dbConnection, ref transaction, link, rap);

                        current++;
                        count++;

                        progress.Report((current * 100) / total);

                        link = null;
                    }

                    if (count >= 1000)
                    {
                        transaction.Commit();
                        transaction.Dispose();
                        count = 0;
                        transaction = dbConnection.BeginTransaction();
                    }
                }

                if (!string.IsNullOrEmpty(link))
                {
                    ImportContentsToDb(mw, dbConnection, ref transaction, link);

                    current++;

                    progress.Report((current * 100) / total);
                }

                if (count > 0)
                {
                    transaction.Commit();

                    transaction.Dispose();
                }
            }
        }

        #endregion Importing/Exporting methods

        #region Database methods

        internal static void BackupDatabase(string backupDirectory)
        {
            string backupDb =
                $"{Path.GetFileNameWithoutExtension(App.DBHSettings.DatabasePath)} {DateTime.Now:yy-MM-dd HH.mm.ss}{Path.GetExtension(App.DBHSettings.DatabasePath)}";

            backupDb = Path.Combine(backupDirectory, backupDb);

            using (var dbConnection =
                new SQLiteConnection($"Data Source={App.DBHSettings.DatabasePath}; Version=3"))
            using (var backupDbConnection = new SQLiteConnection($"Data Source={backupDb}; Version=3"))
            {
                dbConnection.Open();
                backupDbConnection.Open();

                dbConnection.BackupDatabase(backupDbConnection, "main", "main", -1, null, 0);
            }
        }

        internal static void CleanupDatabase()
        {
            using (var dbConnection =
                new SQLiteConnection($@"Data Source={App.DBHSettings.DatabasePath}; Version=3"))
            {
                dbConnection.Open();

                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = "vacuum";

                    command.ExecuteNonQuery();
                }
            }
        }

        internal static void CreateDatabase()
        {
            if (!File.Exists(App.DBHSettings.DatabasePath))
            {
                var reply = MessageBox.Show(Resources.SettingsDBNotExists, Resources.AppName,
                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (reply == MessageBoxResult.Yes)
                {
                    SQLiteConnection.CreateFile(App.DBHSettings.DatabasePath);

                    CreateTables();
                }
            }
        }

        internal static async void CreateRaps(IProgress<double> progress)
        {
            await Task.Run(() =>
            {
                var raps = GetRaps();

                int current = 0;
                int total = raps.Count;

                foreach (var rap in raps)
                {
                    RAPFunctions.GenerateRAP(rap, App.DBHSettings.RAPsPath);

                    current++;

                    progress.Report((current * 100) / total);
                }

                MainWindow.AppLog.Add($"[{DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")}] {Resources.GeneratedRAPs}");
            });
        }

        internal static async void DeleteDuplicateEntries(MainWindow mw)
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
                {
                    dbConnection.Open();
                    using (var transaction = dbConnection.BeginTransaction())
                    {
                        foreach (var console in ConsolesToSearch)
                            using (var command = dbConnection.CreateCommand())
                            {
                                var hs = new HashSet<DatabaseEntry>();

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                                command.CommandText = $"SELECT *,ROWID FROM {console}";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                                var reader = command.ExecuteReader();

                                while (reader.Read())
                                {
                                    DatabaseEntry dbEntry = new DatabaseEntry();
                                    dbEntry.CID = reader["cid"].ToString();
                                    dbEntry.Link = reader["link"].ToString();
                                    dbEntry.RAP = reader["rap"].ToString();
                                    dbEntry.RowID = reader["ROWID"].ToString();
                                    dbEntry.Size = (long) reader["size"];
                                    dbEntry.Title = reader["title"].ToString();

                                    hs.Add(dbEntry);
                                }

                                RemoveDuplicates(dbConnection, console, hs);

                                FillMissingRaps(dbConnection, console, hs);
                            }

                        transaction.Commit();
                    }
                }

                RefreshDatabaseEntries(mw);
            });
        }

        internal static async void DeleteEntry(string titleId, string rowId)
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
                {
                    dbConnection.Open();

                    string console = GetConsole(titleId);

                    using (var command = dbConnection.CreateCommand())
                    {
                        if (!console.Any(char.IsLetterOrDigit))
                            throw new Exception("SQL injection");

                        command.CommandText = $"DELETE FROM {console} WHERE ROWID = @rowID";

                        command.Parameters.Add(new SQLiteParameter("@rowID", rowId));

                        command.ExecuteNonQuery();
                    }
                }
            });
        }

        internal static async void DeleteVideoEntry(string link)
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
                {
                    dbConnection.Open();

                    using (var command = dbConnection.CreateCommand())
                    {
                        command.CommandText = "DELETE FROM videos WHERE link = @link";

                        command.Parameters.Add(new SQLiteParameter("@link", link));

                        command.ExecuteNonQuery();
                    }
                }
            });
        }

        internal static async void SearchMainDb(MainWindow mw, string search = null)
        {
            if (!File.Exists(App.DBHSettings.DatabasePath))
                return;

            await Task.Run(() =>
            {
                try
                {
                    using (var dbConnection = new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
                    {
                        dbConnection.Open();

                        bool[] consolesToSearchToggled =
                        {
                            App.DBHSettings.SearchPS3, App.DBHSettings.SearchPS4, App.DBHSettings.SearchPSP,
                            App.DBHSettings.SearchPSV
                        };

                        for (int i = 0; i < ConsolesToSearch.Length; i++)
                            if (consolesToSearchToggled[i])
                                using (var command = dbConnection.CreateCommand())
                                {
                                    if (string.IsNullOrWhiteSpace(search))
                                        command.CommandText = $"SELECT *,ROWID FROM {ConsolesToSearch[i]}";
                                    else
                                    {
                                        if (!ConsolesToSearch[i].Any(char.IsLetterOrDigit))
                                            throw new Exception("SQL injection");

                                        command.CommandText =
                                            $"SELECT *,ROWID FROM {ConsolesToSearch[i]} WHERE lower(title) LIKE @searchLow OR upper(cid) LIKE @searchUp OR link = @search";

                                        command.Parameters.Add(new SQLiteParameter("@searchLow",
                                            $"%{search.ToLower()}%"));
                                        command.Parameters.Add(new SQLiteParameter("@searchUp",
                                            $"%{search.ToUpper().Replace(" ", "")}%"));
                                        command.Parameters.Add(new SQLiteParameter("@search", search));
                                    }

                                    var reader = command.ExecuteReader();

                                    while (reader.Read())
                                    {
                                        var dbEntry = new DatabaseEntry();
                                        dbEntry.CID = reader["cid"].ToString();
                                        dbEntry.Link = reader["link"].ToString();
                                        dbEntry.RAP = reader["rap"].ToString();
                                        dbEntry.RowID = reader["ROWID"].ToString();
                                        dbEntry.Size = (long) reader["size"];
                                        dbEntry.Title = reader["title"].ToString();
                                        dbEntry.TitleID = dbEntry.CID.Substring(7, 9);

                                        Application.Current.Dispatcher.BeginInvoke((Action) delegate
                                        {
                                            mw.DatabaseEntries.Add(dbEntry);
                                        });
                                    }
                                }
                    }
                }
                catch (SQLiteException)
                {
                    App.DBHSettings.DatabasePath = string.Empty;

                    CreateDatabase();
                }

                Application.Current.Dispatcher.BeginInvoke((Action) delegate
                {
                    mw.Title = string.Format(Resources.AppNameCounter, mw.DatabaseEntries.Count);
                });
            });
        }

        internal static async void SearchNewestEntries()
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
                {
                    dbConnection.Open();

                    var results = new List<DatabaseEntry>();

                    bool[] consolesToSearchToggled =
                    {
                        App.DBHSettings.SearchPS3, App.DBHSettings.SearchPS4, App.DBHSettings.SearchPSP,
                        App.DBHSettings.SearchPSV
                    };

                    for (int i = 0; i < ConsolesToSearch.Length; i++)
                        if (consolesToSearchToggled[i])
                            using (var command = dbConnection.CreateCommand())
                            {
                                command.CommandText = $"SELECT * FROM {ConsolesToSearch[i]}";

                                var reader = command.ExecuteReader();

                                var consoleResults = new List<DatabaseEntry>();

                                while (reader.Read())
                                {
                                    var dbEntry = new DatabaseEntry
                                    {
                                        CID = reader["cid"].ToString(),
                                        Link = reader["link"].ToString(),
                                        RAP = reader["rap"].ToString(),
                                        Size = (long) reader["size"],
                                        Title = reader["title"].ToString()
                                    };

                                    consoleResults.Add(dbEntry);
                                }

                                if (consoleResults.Count > App.DBHSettings.LatestEntries)
                                    consoleResults.RemoveRange(0, consoleResults.Count - App.DBHSettings.LatestEntries);

                                results.AddRange(consoleResults);
                            }

                    for (int i = 0; i < results.Count; i++)
                    {
                        int x = i;

                        Application.Current.Dispatcher.BeginInvoke((Action) delegate
                        {
                            MyNewestWindow.DatabaseEntries.Add(results[x]);
                        });
                    }
                }
            });
        }

        internal static async void SearchSimilarEntries()
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
                {
                    dbConnection.Open();

                    for (int i = 0; i < ConsolesToSearch.Length; i++)
                        using (var command = dbConnection.CreateCommand())
                        {
                            var hs = new HashSet<DatabaseEntry>();

                            command.CommandText = $"SELECT *,ROWID FROM {ConsolesToSearch[i]};";

                            var reader = command.ExecuteReader();

                            while (reader.Read())
                            {
                                var dbEntry = new DatabaseEntry
                                {
                                    CID = reader["cid"].ToString(),
                                    Link = reader["link"].ToString(),
                                    RAP = reader["rap"].ToString(),
                                    RowID = reader["ROWID"].ToString(),
                                    Size = (long) reader["size"],
                                    Title = reader["title"].ToString()
                                };
                                dbEntry.TitleID = dbEntry.CID.Substring(7, 9);

                                hs.Add(dbEntry);
                            }

                            var similarEntries = hs.Select(x => new {value = x})
                                .GroupBy(x => new {x.value.CID, x.value.Link, x.value.Size, x.value.Title})
                                .Where(x => x.Skip(1).Any()).ToArray();

                            var filteredSimilarEntries = similarEntries.SelectMany(x => x).ToList();

                            foreach (var anonDbEntry in filteredSimilarEntries)
                                Application.Current.Dispatcher.BeginInvoke((Action) delegate
                                {
                                    MySimilarWindow.DatabaseEntries.Add(anonDbEntry.value);
                                });
                        }
                }
            });
        }

        internal static async void SearchTitleDb(MyTitlesWindow tw, string search = null, bool lidOnly = false)
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
                {
                    dbConnection.Open();
                    dbConnection.BindFunction(new SQLiteRegex());

                    using (var command = dbConnection.CreateCommand())
                    {
                        if (lidOnly && string.IsNullOrWhiteSpace(search))
                        {
                            command.CommandText = "SELECT * FROM titles WHERE title REGEXP @LIDRegex";

                            command.Parameters.Add(new SQLiteParameter("@LIDRegex", LidRegex));
                        }
                        else if (lidOnly && !string.IsNullOrWhiteSpace(search))
                        {
                            command.CommandText =
                                "SELECT * FROM titles WHERE title REGEXP @LIDRegex AND lower(title) LIKE @search OR lower(titleid) LIKE @search";

                            command.Parameters.Add(new SQLiteParameter("@LIDRegex", LidRegex));
                            command.Parameters.Add(new SQLiteParameter("@search", $"%{search.ToLower()}%"));
                        }
                        else if (!lidOnly && string.IsNullOrWhiteSpace(search))
                            command.CommandText = "SELECT * FROM titles";
                        else if (!lidOnly && !string.IsNullOrWhiteSpace(search))
                        {
                            command.CommandText =
                                "SELECT * FROM titles WHERE lower(title) LIKE @search OR lower(titleid) LIKE @search";

                            command.Parameters.Add(new SQLiteParameter("@search", $"%{search.ToLower()}%"));
                        }

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var dbEntry = new TitleDatabaseEntry();
                            dbEntry.Title = reader["title"].ToString();
                            dbEntry.TitleID = reader["titleid"].ToString();

                            Application.Current.Dispatcher.BeginInvoke((Action) delegate
                            {
                                MyTitlesWindow.TitleDatabaseEntries.Add(dbEntry);
                            });
                        }
                    }
                }

                Application.Current.Dispatcher.BeginInvoke((Action) delegate
                {
                    tw.Title = string.Format(Resources.TitlesWindowTitleCounter,
                        MyTitlesWindow.TitleDatabaseEntries.Count);
                });
            });
        }

        internal static async void SearchVideoDB(MyVideosWindow vw, string search = null)
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
                {
                    dbConnection.Open();

                    using (var command = dbConnection.CreateCommand())
                    {
                        if (string.IsNullOrWhiteSpace(search))
                            command.CommandText = "SELECT * FROM videos";
                        else
                        {
                            command.CommandText =
                                "SELECT * FROM videos WHERE lower(titleid) LIKE @searchLower OR lower(title) LIKE @searchLower OR link = @search";

                            command.Parameters.Add(new SQLiteParameter("@search", search));
                            command.Parameters.Add(new SQLiteParameter("@searchLower", $"%{search.ToLower()}%"));
                        }

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var dbEntry = new VideoDatabaseEntry
                            {
                                Title = reader["title"].ToString(),
                                TitleID = reader["titleid"].ToString(),
                                Link = reader["link"].ToString()
                            };

                            Application.Current.Dispatcher.BeginInvoke((Action) delegate
                            {
                                MyVideosWindow.VideoDatabaseEntries.Add(dbEntry);
                            });
                        }
                    }
                }

                Application.Current.Dispatcher.BeginInvoke((Action) delegate
                {
                    vw.Title = string.Format(Resources.VideosWindowTitleCounter,
                        MyVideosWindow.VideoDatabaseEntries.Count);
                });
            });
        }

        internal static async void UpdateEntryTitles(MainWindow mw)
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
                {
                    dbConnection.Open();

                    using (var transaction = dbConnection.BeginTransaction())
                    {
                        var titleData = GetTitles();

                        foreach (var console in ConsolesToSearch)
                            using (var command = dbConnection.CreateCommand())
                            {
                                command.CommandText = $"SELECT *,ROWID FROM {console}";

                                var reader = command.ExecuteReader();

                                while (reader.Read())
                                    using (var updateTitleCommand = dbConnection.CreateCommand())
                                    {
                                        string cid = reader["cid"].ToString();
                                        string rowId = reader["ROWID"].ToString();

                                        string titleId = cid.Substring(7, 9);

                                        if (!console.Any(char.IsLetterOrDigit))
                                            throw new Exception("SQL injection");

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                                        updateTitleCommand.CommandText =
                                            $"UPDATE {console} SET title = @title WHERE ROWID = @rowID";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                                        updateTitleCommand.Parameters.Add(new SQLiteParameter("@title",
                                            titleData[titleId]));
                                        updateTitleCommand.Parameters.Add(new SQLiteParameter("@rowID", rowId));

                                        updateTitleCommand.ExecuteNonQuery();
                                    }
                            }

                        transaction.Commit();
                    }
                }

                RefreshDatabaseEntries(mw);

                MainWindow.AppLog.Add($"[{DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")}] {Resources.UpdatedTitles}");
            });
        }

        internal static async void UpdateLIDTitlesOnline(MainWindow mw, IProgress<double> progress)
        {
            await Task.Run(() =>
            {
                using (var dbConnection =
                    new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
                {
                    dbConnection.Open();
                    dbConnection.BindFunction(new SQLiteRegex());
                    var titleDatabaseEntries = new List<TitleDatabaseEntry>();
                    var cids = new List<string>();
                    using (SQLiteCommand getLidsCommand = dbConnection.CreateCommand())
                        foreach (string console in ConsolesToSearch)
                        {
                            if (!console.Any(char.IsLetterOrDigit))
                                throw new Exception("SQL injection");

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                            getLidsCommand.CommandText =
                                $"SELECT titleid,cid FROM titles,{console} WHERE titles.title REGEXP @LIDRegex AND cid LIKE '%' || titleid || '%' GROUP BY titleid";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities
                            getLidsCommand.Parameters.Add(new SQLiteParameter("@LIDRegex", "^[A-Z\\d_]{16}$"));
                            using (var reader = getLidsCommand.ExecuteReader())
                                while (reader.Read())
                                {
                                    var entry = new TitleDatabaseEntry();
                                    entry.TitleID = reader["titleid"].ToString();
                                    titleDatabaseEntries.Add(entry);
                                    cids.Add(reader["cid"].ToString());
                                }
                        }

                    for (int i = 0; i < titleDatabaseEntries.Count; i++)
                    {
                        if (i % 10 == 0)
                            MainWindow.AppLog.Add(
                                $"[{DateTime.Now.TimeOfDay:hh\\:mm\\:ss}] {string.Format(Resources.UpdateTitleProgress, i + 1, titleDatabaseEntries.Count)}");

                        titleDatabaseEntries[i].Title = PSWebStore.GetPKGTitle(cids[i], true);
                        progress.Report((i + 1) * 100 / titleDatabaseEntries.Count);
                    }

                    using (var transaction = dbConnection.BeginTransaction())
                    using (var updateTitleCommand = dbConnection.CreateCommand())
                    {
                        foreach (TitleDatabaseEntry titleDatabaseEntry in titleDatabaseEntries)
                        {
                            updateTitleCommand.CommandText =
                                "UPDATE titles SET title = @title WHERE titleid = @titleID";
                            updateTitleCommand.Parameters.Add(new SQLiteParameter("@title", titleDatabaseEntry.Title));
                            updateTitleCommand.Parameters.Add(new SQLiteParameter("@titleID",
                                titleDatabaseEntry.TitleID));
                            updateTitleCommand.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                }

                UpdateEntryTitles(mw);
            });
        }

        internal static void UpdateRap(string newRap, DatabaseEntry dbEntry)
        {
            if (!Regex.IsMatch(newRap.ToUpper(), RapRegex) && !string.IsNullOrEmpty(newRap))
                return;

            using (var dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            {
                dbConnection.Open();

                string console = GetConsole(dbEntry.TitleID);

                using (var command = dbConnection.CreateCommand())
                {
                    if (!console.Any(char.IsLetterOrDigit))
                        throw new Exception("SQL injection");

                    command.CommandText = $"UPDATE {console} SET rap = @rap WHERE ROWID = @rowID";

                    command.Parameters.Add(new SQLiteParameter("@rap", newRap.ToUpper()));
                    command.Parameters.Add(new SQLiteParameter("@rowID", dbEntry.RowID));

                    command.ExecuteNonQuery();
                }
            }
        }

        internal static void UpdateTitle(string newTitle, string titleId)
        {
            using (var dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            {
                dbConnection.Open();

                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = "UPDATE titles SET title = @title WHERE titleid = @titleID";

                    command.Parameters.Add(new SQLiteParameter("@title", newTitle));
                    command.Parameters.Add(new SQLiteParameter("@titleID", titleId));

                    command.ExecuteNonQuery();
                }
            }
        }

        internal static void UpdateVideoTitle(string newTitle, string link)
        {
            using (var dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            {
                dbConnection.Open();

                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = "UPDATE videos SET title = @title WHERE link = @link";

                    command.Parameters.Add(new SQLiteParameter("@title", newTitle));
                    command.Parameters.Add(new SQLiteParameter("@link", link));

                    command.ExecuteNonQuery();
                }
            }
        }

        private static void CreateTables()
        {
            CreateDatabase();

            using (var dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            {
                dbConnection.Open();

                using (var transaction = dbConnection.BeginTransaction())
                {
                    foreach (var console in ConsolesToSearch)
                        using (var consoleCommand = dbConnection.CreateCommand())
                        {
                            if (!console.Any(char.IsLetterOrDigit))
                                throw new Exception("SQL injection");

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                            consoleCommand.CommandText =
                                $"CREATE TABLE IF NOT EXISTS {console}(cid TEXT(36), rap TEXT, title TEXT, size INTEGER, link TEXT)";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                            consoleCommand.ExecuteNonQuery();
                        }

                    using (var titlesCommand = dbConnection.CreateCommand())
                    {
                        titlesCommand.CommandText =
                            "CREATE TABLE IF NOT EXISTS titles(titleid TEXT(9) PRIMARY KEY, title TEXT, UNIQUE(titleid)) WITHOUT ROWID";

                        titlesCommand.ExecuteNonQuery();
                    }

                    using (var moviesCommand = dbConnection.CreateCommand())
                    {
                        moviesCommand.CommandText =
                            "CREATE TABLE IF NOT EXISTS videos(titleid TEXT(9), title TEXT, link TEXT PRIMARY KEY, UNIQUE(link)) WITHOUT ROWID";

                        moviesCommand.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }
        }

        private static void FillMissingRaps(SQLiteConnection dbConnection, string console, HashSet<DatabaseEntry> hs)
        {
            /* TEMPORARY WORKING SOLUTION, PENDING CODE REVIEW */

            var contents = hs.ToArray();

            var seenLink = new HashSet<string>();
            var seenEntry = new HashSet<DatabaseEntry>();

            var toDelete = new List<DatabaseEntry>();

            foreach (var content in contents)
            {
                if (!seenLink.Contains(content.Link))
                {
                    seenLink.Add(content.Link);
                    seenEntry.Add(content);
                }
                else
                {
                    foreach (var subDb in seenEntry.Where(subDb => subDb.Link == content.Link))
                    {
                        if (string.IsNullOrWhiteSpace(subDb.RAP) &&
                            !string.IsNullOrWhiteSpace(content
                                .RAP)) // The new entry has a RAP value, which the old entry was missing... keep only the new entry
                        {
                            seenEntry.Remove(subDb);
                            seenEntry.Add(content);

                            toDelete.Add(subDb);

                            break;
                        }

                        if (string.IsNullOrWhiteSpace(content.RAP) &&
                            !string.IsNullOrWhiteSpace(subDb
                                .RAP)) // The new entry is missing its RAP Value, which the old entry already had... keep only the old entry
                        {
                            seenEntry.Remove(content);
                            seenEntry.Add(subDb);

                            toDelete.Add(content);

                            break;
                        }

                        if (subDb.RAP !=
                            content.RAP) // If the RAP values differ, save a copy of both entries to similar
                        {
                            seenEntry.Add(content);

                            break;
                        }

                        if (subDb.RAP ==
                            content.RAP) // Same rap values - remove, but save a copy in auto-removed similar, just in case
                        {
                            seenEntry.Remove(content);
                            toDelete.Add(content);

                            break;
                        }
                    }
                }
            }

            foreach (var entryToDelete in toDelete)
                using (var removeDuplicateCommand = dbConnection.CreateCommand())
                {
                    if (!console.Any(char.IsLetterOrDigit))

                        throw new Exception("SQL injection");

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                    removeDuplicateCommand.CommandText = $"DELETE FROM {console} WHERE ROWID = @rowID";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                    removeDuplicateCommand.Parameters.Add(new SQLiteParameter("@rowID", entryToDelete.RowID));

                    removeDuplicateCommand.ExecuteNonQuery();
                }
        }

        private static List<Tuple<string, string>> GetRaps()
        {
            using (var dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            {
                dbConnection.Open();

                var raps = new List<Tuple<string, string>>();

                foreach (var console in ConsolesToSearch)
                    using (var command = dbConnection.CreateCommand())
                    {
                        if (!console.Any(char.IsLetterOrDigit))
                            throw new Exception("SQL injection");

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                        command.CommandText = $"SELECT cid,rap FROM {console} WHERE rap <> ''";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                            raps.Add(new Tuple<string, string>(reader["cid"].ToString(), reader["rap"].ToString()));
                    }

                return raps;
            }
        }

        private static Dictionary<string, string> GetTitles()
        {
            var titleData = new Dictionary<string, string>();

            using (var dbConnection =
                new SQLiteConnection($@"Data Source = {App.DBHSettings.DatabasePath}; Version = 3"))
            {
                dbConnection.Open();

                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM titles";

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                        titleData.Add(reader["titleid"].ToString(), reader["title"].ToString());
                }
            }

            return titleData;
        }

        private static void RefreshDatabaseEntries(MainWindow mw)
        {
            Application.Current.Dispatcher.BeginInvoke((Action) delegate
            {
                mw.DatabaseEntries.Clear();

                mw.searchTextBox.Text = string.Empty;
            });

            SearchMainDb(mw);
        }

        private static void RefreshDatabaseEntriesRealTime(MainWindow mw, string cid, string rap, string title,
            long size, string link)
        {
            var dbEntry = new DatabaseEntry
            {
                CID = cid,
                RAP = rap,
                Title = title,
                Size = size,
                Link = link
            };

            Application.Current.Dispatcher.BeginInvoke((Action) delegate
            {
                mw.DatabaseEntries.Add(dbEntry);
                mw.Title = string.Format(Resources.AppNameCounter, mw.DatabaseEntries.Count);
            });
        }

        private static void RemoveDuplicates(SQLiteConnection dbConnection, string console, HashSet<DatabaseEntry> hs)
        {
            var duplicates = hs.Select(x => new {value = x})
                .GroupBy(x => new {x.value.CID, x.value.Link, x.value.RAP, x.value.Size, x.value.Title})
                .Where(x => x.Skip(1).Any()).ToArray();

            var filteredDuplicates = duplicates.SelectMany(x => x).ToList();

            var unique = new HashSet<string>();

            for (int i = 0; i < filteredDuplicates.Count;)
            {
                if (!unique.Contains(filteredDuplicates[i].value.Link))
                {
                    unique.Add(filteredDuplicates[i].value.Link);

                    filteredDuplicates.RemoveAt(i);

                    i = 0;
                }
                else
                    i++;
            }

            foreach (var filteredDuplicate in filteredDuplicates)
                using (var removeDuplicateCommand = dbConnection.CreateCommand())
                {
                    if (!console.Any(char.IsLetterOrDigit))
                        throw new Exception("SQL injection");

#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                    removeDuplicateCommand.CommandText = $"DELETE FROM {console} WHERE ROWID = @rowID";
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                    removeDuplicateCommand.Parameters.Add(new SQLiteParameter("@rowID",
                        filteredDuplicate.value.RowID));

                    removeDuplicateCommand.ExecuteNonQuery();
                }
        }

        #endregion Database methods
    }
}