﻿using DB_Handler.Models;
using DB_Handler.Views;
using Extra.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace DB_Handler.Functions
{
    internal class PSWebStore
    {
        #region Fields

        private const string CidRegex = @"[A-Z]{2}\d{4}\-[A-Z]{4}\d{5}_\d{2}\-[\S]{16}";
        private const string SplitPKGRegex = @"_[1-9]\.pkg|_\d[1-9]\.pkg|_[1-9]\d\.pkg";
        private const string TitleIDRegex = @"[A-Z]{4}\d{5}";
        private const string UserAgent = "";
        private static readonly byte[] CNTMagic = {0x7F, 0x43, 0x4E, 0x54}; // PS4
        private static readonly byte[] PKGMagic = {0x7F, 0x50, 0x4B, 0x47}; // PS3, PSP, PSV
        private static readonly byte[] P3TName = {0x00, 0x6E, 0x61, 0x6D, 0x65, 0x00};

        #endregion Fields

        #region Internal methods

        internal static string[] GetJSONInfo(string jsonLink)
        {
            if (jsonLink.EndsWith(".json", StringComparison.InvariantCultureIgnoreCase))
                return GetJSONLinks(jsonLink);
            else // Not a json link. How did we get here?
                return null;
        }

        internal static string[] GetXMLInfo(string xmlLink)
        {
            if (xmlLink.EndsWith(".xml", StringComparison.InvariantCultureIgnoreCase))
                return GetXMLLinks(xmlLink);
            else
                return null;
        }

        internal static PKGInfo GetPKGInfo(string link, bool checkStores = false)
        {
            var pkgInfo = new PKGInfo();

            if (link.EndsWith(".mp4", StringComparison.InvariantCultureIgnoreCase))
            {
                // Video
                try
                {
                    long size = GetPKGSize(link);
                }
                catch (WebException we)
                {
                    File.AppendAllText("error.log", string.Format("{0} {1}", we.Message.Replace(".", ":"), link));

                    if (we.Response != null)
                    {
                        dynamic webExceptionResponse = we.Response;

                        if (webExceptionResponse.StatusCode == HttpStatusCode.NotFound)
                            return null;
                    }

                    return null;
                }

                string title = Path.GetFileNameWithoutExtension(new Uri(link).AbsolutePath);

                title = title.Replace("%20", " ").Replace("_", " ").Replace("\n", " ");

                title = title.Replace("-HD", "").Replace("-HD720", "").Replace("-HD1080", "").Replace("_720p", "")
                    .Replace(" PS3", "");
                title = title.Replace(" HDTV 720p", "").Replace(" 720p", "").Replace("1080", "").Replace("720", "");

                pkgInfo.Title = title;

                pkgInfo.TitleID = GetVideoTitleID(link);

                return pkgInfo;
            }

            if (link.EndsWith(".pkg", StringComparison.InvariantCultureIgnoreCase))
            {
                bool? isPSP3V = IsPSPV3PKG(link);

                if (isPSP3V.HasValue)
                {
                    pkgInfo.CID = GetPKGCid(link, isPSP3V);

                    string titleID = pkgInfo.CID.Substring(7, 1).ToUpper();

                    pkgInfo.IsPS3orPSPPKG =
                        titleID == "B" || titleID == "N" && !link.Contains("/psm/np/NPNA/") || titleID == "U"
                            ? true
                            : false;
                }
                else
                    pkgInfo.CID = Regex.Match(link, CidRegex).Value;

                try
                {
                    pkgInfo.Size = GetPKGSize(link);
                }
                catch (WebException)
                {
                    MainWindow.AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"),
                        string.Format(Properties.Resources.HTTP404NotFound, link)));

                    return null;
                }

                pkgInfo.Title = GetPKGTitle(pkgInfo.CID, checkStores);

                // PS4 or split PS3-PS4 PKG
                if (!isPSP3V.HasValue || !isPSP3V.Value)
                    return pkgInfo;

                // PS3-PSP-PSV PKG
                pkgInfo.FilesAndFolders = GetPKGFilesAndFolders(link);

                pkgInfo.LicenseType = GetPKGLicenseType(link);

                var pkgVersionInfo = GetPKGVersionInformation(link);

                if (!pkgVersionInfo.isValid)
                    return pkgInfo;

                pkgInfo.AppVersion = pkgVersionInfo.appVersion;
                pkgInfo.InstallDirectory = pkgVersionInfo.installDirectory;
                pkgInfo.PackageVersion = pkgVersionInfo.packageVersion;
                pkgInfo.PS3SystemVersion = pkgVersionInfo.ps3SystemVersion;

                return pkgInfo;
            }

            return null;
        }

        public static P3TInfo GetP3TInfo(string link)
        {
            if (link.EndsWith(".p3t", StringComparison.InvariantCultureIgnoreCase))
            {
                var p3TInfo = new P3TInfo();

                int start = link.IndexOf("cdn/", StringComparison.Ordinal) + 4;
                p3TInfo.Cid = link.Substring(start, 32).Replace("/", "-");

                try
                {
                    p3TInfo.Size = GetPKGSize(link);
                }
                catch (WebException)
                {
                    MainWindow.AppLog.Add(
                        $"[{DateTime.Now.TimeOfDay:hh\\:mm\\:ss}] {string.Format(Properties.Resources.HTTP404NotFound, link)}");

                    return null;
                }

                p3TInfo.Title = GetP3TTitle(link);

                return p3TInfo;
            }

            return null;
        }

        private static string GetP3TTitle(string link)
        {
            int headerStart;
            int headerEnd;

            var request = (HttpWebRequest) WebRequest.Create(link);
            request.UserAgent = UserAgent;
            request.AddRange(0x10, 0x23);

            using (var response = request.GetResponse())
            using (var data = response.GetResponseStream())
            using (var reader = new BinaryReader(data))
            {
                byte[] headerStartPosition = reader.ReadBytes(sizeof(int));
                reader.ReadBytes(0xC);
                byte[] headerEndPosition = reader.ReadBytes(sizeof(int));

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(headerStartPosition);
                    Array.Reverse(headerEndPosition);
                }

                headerStart = BitConverter.ToInt32(headerStartPosition, 0);
                headerEnd = BitConverter.ToInt32(headerEndPosition, 0);
            }

            byte[] header;

            request = (HttpWebRequest) WebRequest.Create(link);
            request.UserAgent = UserAgent;
            request.AddRange(headerStart, headerEnd);

            using (var response = request.GetResponse())
            using (var data = response.GetResponseStream())
            using (var reader = new BinaryReader(data))
                header = reader.ReadBytes(headerEnd - headerStart);

            int nameStart = (from index in Enumerable.Range(0, header.Length - P3TName.Length + 1)
                where header.Skip(index).Take(P3TName.Length).SequenceEqual(P3TName)
                select index).First() + P3TName.Length;
            int nameEnd = Array.IndexOf(header, (byte) 0, nameStart);
            byte[] name = new byte[nameEnd - nameStart];
            Array.Copy(header, nameStart, name, 0, name.Length);
            return Encoding.UTF8.GetString(name);
        }

        #region CID Methods

        internal static string GetPKGTitle(string cid, bool checkStores = false)
        {
            if (!checkStores)
                using (var dbConnection =
                    new SQLiteConnection(@"Data Source = " + App.DBHSettings.DatabasePath + "; Version = 3"))
                {
                    dbConnection.Open();

                    using (var command = dbConnection.CreateCommand())
                    {
                        command.CommandText = "SELECT * FROM titles WHERE titleid = @titleID";

                        command.Parameters.Add(new SQLiteParameter("@titleID", cid.Substring(7, 9)));

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                            return reader["title"].ToString();
                    }
                }

            if (!checkStores)
            {
                string titleName = TMDBUtil.GetTitle(cid.Substring(7, 9));

                if (!string.IsNullOrWhiteSpace(titleName))
                    return titleName;
            }

            var storeLinksAndLanguage = GetStoreLinks(cid);

            if (storeLinksAndLanguage.isValid)
            {
                var request = new WebClient();
                request.Headers.Add("User-Agent", UserAgent);

                using (var stream = request.OpenRead(storeLinksAndLanguage.jsonLink))
                using (var reader = new StreamReader(stream))
                {
                    var json = JObject.Parse(reader.ReadToEnd());

                    JToken result;
                    bool jsonParsed = json.TryGetValue("title_name", out result);

                    if (!jsonParsed)
                        result = json["parent_links"][0]["name"];

                    return result.ToString().Trim().Replace("\n", "");
                }
            }
            else
                // No luck, return LID
                return cid.Substring(20, 16);
        }

        internal static string GetVideoTitleID(string link)
        {
            return Regex.Match(link, TitleIDRegex).Value;
        }

        #endregion CID Methods

        #endregion Internal methods

        #region JSON Methods

        private static string[] GetJSONLinks(string jsonLink)
        {
            var request = new WebClient();
            request.Headers.Add("User-Agent", UserAgent);

            try
            {
                using (var stream = request.OpenRead(jsonLink))
                using (var reader = new StreamReader(stream))
                {
                    var json = JObject.Parse(reader.ReadToEnd());

                    int numberOfSplitFiles = (int) json["numberOfSplitFiles"];

                    var links = new List<string>();

                    for (int i = 0; i < numberOfSplitFiles; i++)
                        links.Add(json["pieces"][i]["url"].ToString());

                    return links.ToArray();
                }
            }
            catch (WebException)
            {
                return null;
            }
        }

        #endregion JSON Methods

        #region XML Methods

        private static string[] GetXMLLinks(string xmlLink)
        {
            var request = new WebClient();
            request.Headers.Add("User-Agent", UserAgent);

            try
            {
                using (var stream = request.OpenRead(xmlLink))
                using (var reader = new StreamReader(stream))
                {
                    var xml = XDocument.Parse(reader.ReadToEnd());

                    int numberOfSplitFiles;
                    if (!int.TryParse(xml.Root.Element("number_of_split_files").Value, out numberOfSplitFiles))
                        return null;

                    return xml.Root.Elements("pieces").Select(x => x.Attribute("url").Value).ToArray();
                }
            }
            catch (WebException)
            {
                return null;
            }
        }

        #endregion XML Methods

        #region PKG Methods

        internal static string GetPKGCid(string link, bool? isPSPV3 = null)
        {
            var request = (HttpWebRequest) WebRequest.Create(link);
            request.UserAgent = UserAgent;

            if (!isPSPV3.HasValue)
                isPSPV3 = IsPSPV3PKG(link);

            if (isPSPV3.Value)
                request.AddRange(0x30, 0x53);
            else
                request.AddRange(0x40, 0x63);

            using (var response = request.GetResponse())
            using (var data = response.GetResponseStream())
            using (var reader = new BinaryReader(data))
            {
                byte[] cidRaw = reader.ReadBytes(0x24);

                return Encoding.UTF8.GetString(cidRaw);
            }
        }

        private static int GetPKGFilesAndFolders(string link)
        {
            var request = (HttpWebRequest) WebRequest.Create(link);
            request.UserAgent = UserAgent;
            request.AddRange(0x14, 0x17);

            using (var response = request.GetResponse())
            using (var data = response.GetResponseStream())
            using (var reader = new BinaryReader(data))
            {
                byte[] filesAndFoldersRaw = reader.ReadBytes(0x4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(filesAndFoldersRaw);

                return BitConverter.ToInt32(filesAndFoldersRaw, 0);
            }
        }

        private static byte GetPKGLicenseType(string link)
        {
            var preLicenseChk = (HttpWebRequest) WebRequest.Create(link);
            preLicenseChk.UserAgent = UserAgent;
            preLicenseChk.AddRange(0x8, 0xB);

            int offset;

            using (var response = preLicenseChk.GetResponse())
            using (var data = response.GetResponseStream())
            using (var reader = new BinaryReader(data))
            {
                byte[] rawOffset = reader.ReadBytes(0x4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(rawOffset);

                offset = BitConverter.ToInt32(rawOffset, 0);
            }

            var licenseCheck = (HttpWebRequest) WebRequest.Create(link);
            licenseCheck.UserAgent = UserAgent;
            licenseCheck.AddRange(offset + 0xB, offset + 0xC);

            using (var response = licenseCheck.GetResponse())
            using (var data = response.GetResponseStream())
            using (var reader = new BinaryReader(data))
                return reader.ReadByte();
        }

        private static long GetPKGSize(string link)
        {
            var sizeChk = (HttpWebRequest) WebRequest.Create(link);
            sizeChk.Method = "HEAD"; // Defaults to GET
            sizeChk.UserAgent = UserAgent;

            using (var response = sizeChk.GetResponse())
                return response.ContentLength;
        }

        private static PKGVersionInformation GetPKGVersionInformation(string link)
        {
            var request = (HttpWebRequest) WebRequest.Create(link);
            request.UserAgent = UserAgent;
            request.AddRange(0x13, 0x322);

            using (var response = request.GetResponse())
            using (var data = response.GetResponseStream())
            using (var reader = new BinaryReader(data))
            {
                byte versionCheck = reader.ReadByte();
                // At this point, 0x14 bytes have been consumed. Subtract 0x14 from the absolute offset. For example, 0x109 becomes 0xF5.

                var pkgVersionInfo = new PKGVersionInformation();
                pkgVersionInfo.isValid = true;

                switch (versionCheck)
                {
                    case 0xA0:
                        reader.ReadBytes(0xF5);
                        ReadPKGVersionData(reader, ref pkgVersionInfo);
                        break;

                    case 0xC0:
                        reader.ReadBytes(0x115);
                        ReadPKGVersionData(reader, ref pkgVersionInfo);
                        break;

                    case 0xD0:
                        reader.ReadBytes(0xF5);
                        ReadPKGVersionData(reader, ref pkgVersionInfo);

                        reader.ReadBytes(0x20);
                        pkgVersionInfo.installDirectory = Encoding.UTF8.GetString(reader.ReadBytes(0x20));
                        break;

                    case 0xE0:
                        reader.ReadBytes(0x2E9);
                        ReadPKGVersionData(reader, ref pkgVersionInfo);
                        break;

                    case 0xF0:
                        reader.ReadBytes(0x115);
                        ReadPKGVersionData(reader, ref pkgVersionInfo);

                        reader.ReadBytes(0x20);
                        pkgVersionInfo.installDirectory = Encoding.UTF8.GetString(reader.ReadBytes(0x20));
                        break;

                    default: // "n/a", not available, unknown
                        pkgVersionInfo.isValid = false;
                        return pkgVersionInfo;
                }

                if (string.IsNullOrWhiteSpace(pkgVersionInfo.installDirectory) ||
                    pkgVersionInfo.installDirectory.Contains("\0\0\0"))
                    pkgVersionInfo.installDirectory = string.Empty;

                return pkgVersionInfo;
            }
        }

        private static bool? IsPSPV3PKG(string link)
        {
            return Retry.Do<bool?>(() =>
            {
                var request = (HttpWebRequest) WebRequest.Create(link);
                request.UserAgent = UserAgent;
                request.AddRange(0, 4);

                try
                {
                    using (var response = request.GetResponse())
                    using (var data = response.GetResponseStream())
                    using (var reader = new BinaryReader(data))
                    {
                        byte[] magic = reader.ReadBytes(4);

                        if (Enumerable.SequenceEqual(magic, PKGMagic))
                            return true;
                        else if (Enumerable.SequenceEqual(magic, CNTMagic))
                            return false;
                        else
                            return null;
                    }
                }
                catch (WebException)
                {
                    return null;
                }
            }, 5000, 5);
        }

        private static void ReadPKGVersionData(BinaryReader reader, ref PKGVersionInformation pkgVersionInfo)
        {
            byte[] ps3SystemVersionRaw = reader.ReadBytes(0x2);
            reader.ReadByte();

            pkgVersionInfo.ps3SystemVersion = BitConverter.ToString(ps3SystemVersionRaw).Replace("-", ".");

            if (pkgVersionInfo.ps3SystemVersion == "00.00")
                pkgVersionInfo.ps3SystemVersion = string.Empty;

            byte[] packageVersionRaw = reader.ReadBytes(0x2);

            pkgVersionInfo.packageVersion = BitConverter.ToString(packageVersionRaw).Replace("-", ".");

            if (pkgVersionInfo.packageVersion == "00.00")
                pkgVersionInfo.packageVersion = string.Empty;

            byte[] appVersionRaw = reader.ReadBytes(0x2);

            pkgVersionInfo.appVersion = BitConverter.ToString(appVersionRaw).Replace("-", ".");

            if (pkgVersionInfo.appVersion == "00.00")
                pkgVersionInfo.appVersion = string.Empty;
        }

        #endregion PKG Methods

        #region Web Store Methods

        internal static StoreInformation GetStoreLinks(string cid)
        {
            string[] linkCodes, checkLinkCodes;

            var storeInfo = new StoreInformation();

            bool? titleExists = TMDBUtil.GetTitleExists(cid.Substring(7, 9));

            if (titleExists.HasValue && !titleExists.Value)
                return storeInfo;

            switch (cid[0])
            {
                case 'U': // US
                    // In order:
                    // US, Hong Kong, Brazil
                    linkCodes = new string[] {"en-us", "en-hk", "en-br"};
                    checkLinkCodes = new string[] {"US/en", "HK/en", "BR/en"};

                    for (int i = 0; i < linkCodes.Length; i++)
                    {
                        storeInfo.storeLink = "https://store.playstation.com/#!/" + linkCodes[i] + "/cid=" + cid;
                        storeInfo.jsonLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" +
                                             checkLinkCodes[i] + "/999/" + cid;

                        storeInfo.isValid = IsValidLink(storeInfo.jsonLink);

                        if (storeInfo.isValid)
                        {
                            storeInfo.isEnglish = true;
                            break;
                        }
                    }

                    break;

                case 'J': // Japan
                    // In order:
                    // Hong Kong, Japan
                    linkCodes = new string[] {"en-hk", "ja-jp"};
                    checkLinkCodes = new string[] {"HK/en", "JP/ja"};

                    for (int i = 0; i < linkCodes.Length; i++)
                    {
                        storeInfo.storeLink = "https://store.playstation.com/#!/" + linkCodes[i] + "/cid=" + cid;
                        storeInfo.jsonLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" +
                                             checkLinkCodes[i] + "/999/" + cid;

                        storeInfo.isValid = IsValidLink(storeInfo.jsonLink);

                        if (storeInfo.isValid)
                        {
                            storeInfo.isEnglish = true;
                            break;
                        }
                    }

                    break;

                case 'E': // Europe
                    // In order (English):
                    // UK, Bulgaria, Croatia, Cyprus, Czech Rebublic, Denmark, Finland, Greece, Hungary, Iceland, Ireland,
                    // Israel, Norway, Malta, Romania, Poland, Slovakia, Slovenia, Sweden, Turkey, Hong Kong
                    string[] linkCodesEnglish =
                    {
                        "en-gb", "en-bg", "en-hr", "en-cy", "en-cz", "en-dk", "en-fi", "en-gr", "en-hu", "en-is",
                        "en-ie",
                        "en-il", "en-no", "en-mt", "en-ro", "en-pl", "en-sk", "en-si", "en-se", "en-tr", "en-hk"
                    };
                    string[] checkLinkCodesEnglish =
                    {
                        "GB/en", "BG/en", "HR/en", "CY/en", "CZ/en", "DK/en", "FI/en", "GR/en", "HU/en", "IS/en",
                        "IE/en",
                        "IL/en", "NO/en", "MT/en", "RO/en", "PL/en", "SK/en", "SI/en", "SE/en", "TR/en", "HK/en"
                    };

                    // In order (Local):
                    // Spain (Spanish), France (French), Italy (Italian), Germany (German), Russia (Russian), Belgium (Dutch), Belgium (French), Denmark (Danish),
                    // Luxembourg (French), Luxembourg (German), Netherlands (Dutch), Norway (Norwegian), Austria (German), Poland (Polish), Portugal (Portuguese),
                    // Switzerland (German), Switzerland (French), Finland (Finnish), Sweden (Swedish), Switzerland (Italian), Turkey (Turkish), Ukraine (Russian)
                    string[] linkCodesLocal =
                    {
                        "es-es", "fr-fr", "it-it", "de-de", "ru-ru", "nl-be", "fr-be", "da-dk",
                        "fr-lu", "de-lu", "nl-nl", "no-no", "de-at", "pl-pl", "pt-pt",
                        "de-ch", "fr-ch", "fi-fi", "sv-se", "it-ch", "tr-tr", "ru-ua"
                    };
                    string[] checkLinkCodesLocal =
                    {
                        "ES/es", "FR/fr", "IT/it", "DE/de", "RU/ru", "BE/nl", "BE/fr", "DK/da",
                        "LU/fr", "LU/de", "NL/nl", "NO/no", "AT/de", "PL/pl", "PT/pt",
                        "CH/de", "CH/fr", "FI/fi", "SE/sv", "CH/it", "TR/tr", "UA/ru"
                    };

                    for (int i = 0; i < linkCodesEnglish.Length; i++)
                    {
                        storeInfo.storeLink = "https://store.playstation.com/#!/" + linkCodesEnglish[i] + "/cid=" + cid;
                        storeInfo.jsonLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" +
                                             checkLinkCodesEnglish[i] + "/999/" + cid;

                        storeInfo.isValid = IsValidLink(storeInfo.jsonLink);

                        if (storeInfo.isValid)
                        {
                            storeInfo.isEnglish = true;
                            break;
                        }
                    }

                    if (!storeInfo.isValid)
                        for (int i = 0; i < linkCodesLocal.Length; i++)
                        {
                            storeInfo.storeLink =
                                "https://store.playstation.com/#!/" + linkCodesLocal[i] + "/cid=" + cid;
                            storeInfo.jsonLink =
                                "https://store.playstation.com/store/api/chihiro/00_09_000/container/" +
                                checkLinkCodesLocal[i] + "/999/" + cid;

                            storeInfo.isValid = IsValidLink(storeInfo.jsonLink);

                            if (storeInfo.isValid)
                            {
                                storeInfo.isEnglish = false;
                                break;
                            }
                        }

                    break;

                case 'H': // Hong Kong
                    storeInfo.storeLink = "https://store.playstation.com/#!/en-hk/cid=" + cid;
                    storeInfo.jsonLink =
                        "https://store.playstation.com/store/api/chihiro/00_09_000/container/HK/en/999/" + cid;
                    storeInfo.isValid = IsValidLink(storeInfo.jsonLink);

                    if (storeInfo.isValid)
                        storeInfo.isEnglish = true;

                    break;

                case 'K': // Asia
                    // In order:
                    // Hong Kong, Singapore
                    linkCodes = new string[] {"en-hk", "en-sg"};
                    checkLinkCodes = new string[] {"HK/en", "SG/en"};

                    for (int i = 0; i < linkCodes.Length; i++)
                    {
                        storeInfo.storeLink = "https://store.playstation.com/#!/" + linkCodes[i] + "/cid=" + cid;
                        storeInfo.jsonLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" +
                                             checkLinkCodes[i] + "/999/" + cid;

                        storeInfo.isValid = IsValidLink(storeInfo.jsonLink);

                        if (storeInfo.isValid)
                        {
                            storeInfo.isEnglish = true;
                            break;
                        }
                    }

                    break;
            }

            return storeInfo;
        }

        private static bool IsValidLink(string link)
        {
            var header = (HttpWebRequest) WebRequest.Create(link);
            header.Method = "HEAD"; // Defaults to GET
            header.UserAgent = UserAgent;

            try
            {
                using (header.GetResponse())
                    return true;
            }
            catch (WebException)
            {
                return false;
            }
        }

        #endregion Web Store Methods

        #region Structs

        internal struct StoreInformation
        {
            internal bool isEnglish;
            internal bool isValid;
            internal string jsonLink;
            internal string storeLink;
        }

        private struct PKGVersionInformation
        {
            internal string appVersion;
            internal string installDirectory;
            internal bool isValid;
            internal string packageVersion;
            internal string ps3SystemVersion;
        }

        #endregion Structs
    }
}