﻿using DB_Handler.Converters;
using DB_Handler.Functions;
using DB_Handler.Models;
using Extra.Controls;
using Extra.Functions;
using Extra.Models;
using Extra.Utilities;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace DB_Handler.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Properties

        private const string BackupPath = "Backups";
        private const string RAPRegex = @"[a-fA-F\d]{32}";

        private MyAppLogWindow appLogWindow;
        private Downloader downloader;

        private Timer timer;
        public static ObservableCollection<string> AppLog { get; set; }

        #endregion Properties

        #region Constructor

        public MainWindow()
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            Style = FindResource("MyMainWindowStyle") as Style;

            AppLog = new ObservableCollection<string>();

            downloader = new Downloader(App.DBHSettings.DownloadsPath);
            downloader.OnUpdateStatus += Downloader_OnUpdateStatus;

            timer = new Timer();
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 5000;

            DatabaseEntries = new ObservableCollection<DatabaseEntry>();

            if (App.DBHSettings.DeveloperMode)
                DatabaseEntries.CollectionChanged += DatabaseEntries_CollectionChanged;

            Notifications = new ObservableCollection<string>();
            Notifications.Add(string.Empty);

            PBProgress = new ObservableCollection<double>();
            PBProgress.Add(0);

            DataContext = App.DBHSettings;

            InitializeComponent();

            if (!File.Exists(App.DBHSettings.DatabasePath))
                DatabaseFunctions.CreateDatabase();

            DatabaseFunctions.SearchMainDb(this);

            AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.FinishedLoading));
        }

        #endregion Constructor

        #region Fields

        public ObservableCollection<DatabaseEntry> DatabaseEntries { get; set; }

        public ObservableCollection<string> Notifications { get; set; }

        public ObservableCollection<double> PBProgress { get; set; }

        #endregion Fields

        #region CanExecute

        private void CheckUpdates_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists("Updater.exe");
        }

        private void IfDBExists_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists(App.DBHSettings.DatabasePath);
        }

        #endregion CanExecute

        #region Checkbox selection

        private void CheckBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var dgRowCheckBox = sender as CheckBox;

            var dgRow = VisualTreeHelpers.FindAncestor<DataGridRow>(dgRowCheckBox);

            bool newValue = !dgRowCheckBox.IsChecked.GetValueOrDefault();

            dgRow.IsSelected = newValue;
            dgRowCheckBox.IsChecked = newValue;

            // Mark event as handled so that the default
            // DataGridPreviewMouseDown doesn't handle the event
            e.Handled = true;
        }

        private void DataGridHeaderSelection_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (DatabaseEntries.Count == 0)
            {
                (e.OriginalSource as CheckBox).IsChecked = false;
                return;
            }

            for (int i = 0; i < DatabaseEntries.Count; i++)
                DatabaseEntries[i].IsSelected = (e.OriginalSource as CheckBox).IsChecked.Value;
        }

        private void DataGridSpaceSelect_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selection = (sender as DataGrid).SelectedItems;

            foreach (var entry in selection)
                (entry as DatabaseEntry).IsSelected = !(entry as DatabaseEntry).IsSelected;
        }

        #endregion Checkbox selection

        #region Events

        private void DatabaseEntries_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                if (MessageBox.Show(string.Format(Properties.Resources.DEVWarningDeleteEntry, (e.OldItems[0] as DatabaseEntry).Title, (e.OldItems[0] as DatabaseEntry).CID),
                                    Properties.Resources.AppName, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    DatabaseFunctions.SearchMainDb(this);

                    return;
                }

                var dbEntry = (e.OldItems[0] as DatabaseEntry);

                string titleID = dbEntry.CID.Substring(7, 9);

                DatabaseFunctions.DeleteEntry(titleID, dbEntry.RowID);

                AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format(Properties.Resources.Deleted, dbEntry.Title, dbEntry.CID)));
            }
        }

        private void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit && e.EditingElement.IsFocused)
            {
                var dg = (sender as DataGrid);

                var dbEntry = dg.CurrentItem as DatabaseEntry;

                string newRAP = (e.EditingElement as TextBox).Text;

                string oldRAP = dbEntry.RAP;

                if (dbEntry == null || (!Regex.IsMatch(newRAP.ToUpper(), RAPRegex) && !string.IsNullOrEmpty(newRAP)))
                {
                    dg.CancelEdit();

                    return;
                }

                DatabaseFunctions.UpdateRap(newRAP, dbEntry);

                string rapFile = Path.Combine(App.DBHSettings.RAPsPath, dbEntry.CID + ".rap");

                if (string.IsNullOrEmpty(newRAP) && File.Exists(rapFile))
                    File.Delete(rapFile);

                AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format(Properties.Resources.UpdatedRAP, dbEntry.CID, oldRAP, newRAP)));
            }
            else if (e.EditAction == DataGridEditAction.Commit && !e.EditingElement.IsFocused)
                (sender as DataGrid).CancelEdit();
        }

        private void Downloader_OnUpdateStatus(object sender, DownloadStatus e)
        {
            Notifications[0] = string.Format("{0} {1}", e.Status, MiscUtils.Truncate(Path.GetFileName(e.Filename), 80));

            PBProgress[0] = e.LosslessProgress;

            if (e.LosslessProgress == 100)
                timer.Start();
            else
                timer.Stop();
        }

        private void OnHyperlink_Click(object sender, RoutedEventArgs e)
        {
            var destination = ((Hyperlink)e.OriginalSource).NavigateUri;
            Process.Start(destination.ToString());

            (e.OriginalSource as Hyperlink).Foreground = Brushes.Purple;
        }

        private void OnStartDownload_Click(object sender, RoutedEventArgs e)
        {
            if (Notifications[0].Contains(Properties.Resources.Downloading))
            {
                downloader.CancelDownload();

                return;
            }

            var selectionList = new List<string>();

            for (int i = 0; i < DatabaseEntries.Count; i++)
                if (DatabaseEntries[i].IsSelected)
                {
                    DatabaseEntries[i].IsSelected = false;

                    selectionList.Add(DatabaseEntries[i].Link);
                }

            downloader.DownloadFiles(selectionList);
        }

        private void OnViewInfo_Click(object sender, RoutedEventArgs e)
        {
            var selectionList = new List<DatabaseEntry>();

            for (int i = 0; i < DatabaseEntries.Count; i++)
                if (DatabaseEntries[i].IsSelected)
                    selectionList.Add(DatabaseEntries[i]);

            if (selectionList.Count > 10)
                return;

            for (int i = 0; i < selectionList.Count; i++)
            {
                var infoWindow = new MyImageWindow(selectionList[i]);
                infoWindow.Owner = this;
                infoWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                infoWindow.Show();
            }
        }

        private void TextBox_Search(object sender, RoutedEventArgs e)
        {
            var dg = VisualTreeHelpers.FindChild<DataGrid>(this);

            (dg.Columns[0].Header as CheckBox).IsChecked = false;

            DatabaseEntries.Clear();

            DatabaseFunctions.SearchMainDb(this, (sender as SearchTextBox).Text);
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Notifications[0] = string.Empty;

            PBProgress[0] = 0;

            (sender as Timer).Stop();
        }

        #endregion Events

        #region Executed

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            MessageBox.Show(string.Format(Properties.Resources.AboutMessage, thisAssembly.GetName().Version), Properties.Resources.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Backup_Database()
        {
            if (!Directory.Exists(BackupPath))
                Directory.CreateDirectory(BackupPath);

            var files = new DirectoryInfo(BackupPath).GetFiles("*.db");

            for (int i = 0; i < (files.Length - 4); i++)
                files[i].Delete();

            if (File.Exists(App.DBHSettings.DatabasePath))
                DatabaseFunctions.BackupDatabase(BackupPath);
        }

        private void CheckUpdates_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            Process.Start("Updater.exe", string.Format("\"{0}\" \"{1}\" \"{2}\"", thisAssembly.GetName().Name, thisAssembly.GetName().Version, Process.GetCurrentProcess().MainModule.FileName));
        }

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dataGrid = sender as DataGrid;

            var selection = new StringBuilder();

            var sizeToString = new SizeToString();

            for (int i = 0; i < DatabaseEntries.Count; i++)
                if (DatabaseEntries[i].IsSelected)
                {
                    selection.AppendLine(string.Format(new CultureInfo("es-es"), "{0} [{1} ({2:#,###} B)]", DatabaseEntries[i].Title, sizeToString.Convert(DatabaseEntries[i].Size, null, null, null), DatabaseEntries[i].Size));

                    selection.AppendLine(DatabaseEntries[i].Link);

                    if (!string.IsNullOrEmpty(DatabaseEntries[i].RAP))
                    {
                        selection.AppendLine(DatabaseEntries[i].CID + ".rap");

                        selection.AppendLine(DatabaseEntries[i].RAP);
                    }

                    if (i < DatabaseEntries.Count - 1)
                        selection.AppendLine();
                }

            Clipboard.SetText(selection.ToString());
        }

        private void DataGridScrollToBottom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dg = sender as DataGrid;

            dg.SelectedItem = dg.Items.GetItemAt(dg.Items.Count - 1);

            dg.ScrollIntoView(dg.SelectedItem, dg.CurrentColumn);
        }

        private void DeleteDuplicates_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DatabaseFunctions.DeleteDuplicateEntries(this);
        }

        private void DEVCleanupDB_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show(Properties.Resources.DEVWarningCleanupDB, Properties.Resources.AppName, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                return;

            DatabaseFunctions.CleanupDatabase();
        }

        private void DEVExportTitles_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = Properties.Resources.DEVExportFilter;

            if (sfd.ShowDialog().Value)
                DatabaseFunctions.ExportTitles(sfd.FileName);

            AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.TitlesDBExported));
        }

        private void DEVImportTitles_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show(Properties.Resources.DEVWarningImportTitles, Properties.Resources.AppName, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                return;

            var ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            ofd.Title = Properties.Resources.DEVBrowseDialogSelectTitlesDB;

            if (ofd.ShowDialog().Value)
            {
                string[] titlesDBContents = File.ReadAllLines(ofd.FileName);

                DatabaseFunctions.ImportTitles(titlesDBContents);

                AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.TitlesDBImported));
            }
        }

        private void DEVUpdateTitlesOnline_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show(Properties.Resources.DEVWarningUpdateTitlesOnline, Properties.Resources.AppName, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                return;

            IProgress<double> progress = new Progress<double>(UpdateProgress);

            AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.UpdatingTitles));

            DatabaseFunctions.UpdateLIDTitlesOnline(this, progress);
        }

        private void Donation_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string reference = "DBH";

            Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=serenitydiary%40gmail%2ecom&lc=ES&item_name=Donate%20to%20Dasanko&item_number=" + reference + "&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted");
        }

        private void Export_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = Properties.Resources.ExportFilter;

            if (sfd.ShowDialog().Value)
                DatabaseFunctions.ExportDb(sfd.FileName);
        }

        private void GenerateRAPs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IProgress<double> progress = new Progress<double>(UpdateProgress);

            AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.GeneratingRAPs));

            DatabaseFunctions.CreateRaps(progress);
        }

        private void Import_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = Properties.Resources.ImportFilter;

            if (ofd.ShowDialog().Value)
            {
                if (new FileInfo(ofd.FileName).Length == 0)
                    return;

                IProgress<double> progress = new Progress<double>(UpdateProgress);

                AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.ImportingContents));

                DatabaseFunctions.Import(this, ofd.FileName, progress);
            }
        }

        private void MergeSplitFiles_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var mergeFilesWindow = new MyMergeWindow();
            mergeFilesWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            mergeFilesWindow.Show();
        }

        private void OpenAppLog_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (appLogWindow == null || !appLogWindow.IsVisible)
            {
                appLogWindow = new MyAppLogWindow();
                appLogWindow.Owner = this;
                appLogWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                appLogWindow.Show();
            }
        }

        private void OpenDownloadsFolder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!Directory.Exists(App.DBHSettings.DownloadsPath))
                Directory.CreateDirectory(App.DBHSettings.DownloadsPath);

            Process.Start(App.DBHSettings.DownloadsPath);
        }

        private void OpenRAPsFolder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!Directory.Exists(App.DBHSettings.RAPsPath))
                Directory.CreateDirectory(App.DBHSettings.RAPsPath);

            Process.Start(App.DBHSettings.RAPsPath);
        }

        private void OpenTitlesDB_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var titlesWindow = new MyTitlesWindow();
            titlesWindow.Owner = this;
            titlesWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            titlesWindow.ShowDialog();
        }

        private void OpenVideosDB_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var videosWindow = new MyVideosWindow();
            videosWindow.Owner = this;
            videosWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            videosWindow.ShowDialog();
        }

        private void PasteImport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IProgress<double> progress = new Progress<double>(UpdateProgress);

            AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.ImportingContents));

            DatabaseFunctions.Import(this, Clipboard.GetText(TextDataFormat.Text), progress);
        }

        private void SearchConsoleToggle_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DatabaseEntries.Clear();

            DatabaseFunctions.SearchMainDb(this, searchTextBox.Text);
        }

        private void SelectAll_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dataGrid = sender as DataGrid;

            dataGrid.SelectedIndex = 0;
            dataGrid.SelectAll();
        }

        private void Settings_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var settingsWindow = new MySettingsWindow(this);
            settingsWindow.Owner = this;
            settingsWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settingsWindow.ShowDialog();
        }

        private void ShowNewest_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var nw = new MyNewestWindow();
            nw.Owner = this;
            nw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            nw.ShowDialog();
        }

        private void ShowSimilar_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var similarWindow = new MySimilarWindow();
            similarWindow.Owner = this;
            similarWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            similarWindow.Height = Height / 2;
            similarWindow.ShowDialog();
        }

        private void UpdateProgress(double obj)
        {
            PBProgress[0] = obj;
        }

        private void UpdateTitles_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), Properties.Resources.UpdatingTitles));

            DatabaseFunctions.UpdateEntryTitles(this);
        }

        #endregion Executed

        #region Window States

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            App.WriteDBHSettings(this);

            Backup_Database();

            Application.Current.Shutdown(0);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Height = App.SettingsFile.ReadSetting("Window", "Height", Height);
            Left = App.SettingsFile.ReadSetting("Window", "Left", Left);
            WindowState = App.SettingsFile.ReadSetting("Window", "Maximized", false) ? WindowState.Maximized : WindowState.Normal;
            Top = App.SettingsFile.ReadSetting("Window", "Top", Top);
            Width = App.SettingsFile.ReadSetting("Window", "Width", Width);
        }

        #endregion Window States
    }
}