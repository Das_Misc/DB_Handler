﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DB_Handler.Converters
{
    internal class SizeToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is long)
            {
                string[] sizeMeasurements = { "GiB", "MiB", "KiB", "B" };
                int[] sizeLimits = { 1024 * 1024 * 1024, 1024 * 1024, 1024, 0 };

                for (int i = 0; i < sizeLimits.Length; i++)
                    if ((long)value >= sizeLimits[i])
                    {
                        string size = string.Format(new CultureInfo("es-es"), "{0:#,##0.##} {1}", (double)((long)value) / sizeLimits[i], sizeMeasurements[i]);

                        return size;
                    }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
