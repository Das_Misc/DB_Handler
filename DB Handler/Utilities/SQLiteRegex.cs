﻿using System;
using System.Data.SQLite;
using System.Linq;
using System.Text.RegularExpressions;

namespace Extra.Utilities
{
    internal static class MyRegExBinding
    {
        public static void BindFunction(this SQLiteConnection connection, SQLiteFunction function)
        {
            var attributes = function.GetType().GetCustomAttributes(typeof(SQLiteFunctionAttribute), true).Cast<SQLiteFunctionAttribute>().ToArray();

            if (attributes.Length == 0)
                throw new InvalidOperationException("SQLiteFunction doesn't have SQLiteFunctionAttribute");

            connection.BindFunction(attributes[0], function);
        }
    }

    [SQLiteFunction(Name = "REGEXP", Arguments = 2, FuncType = FunctionType.Scalar)]
    internal class SQLiteRegex : SQLiteFunction
    {
        public override object Invoke(object[] args)
        {
            return Regex.IsMatch(Convert.ToString(args[1]), Convert.ToString(args[0]));
        }
    }

    [SQLiteFunction(Name = "REGEXPReplace", Arguments = 3, FuncType = FunctionType.Scalar)]
    internal class SQLiteRegexReplace : SQLiteFunction
    {
        public override object Invoke(object[] args)
        {
            return Regex.Replace(Convert.ToString(args[0]), Convert.ToString(args[1]), Convert.ToString(args[2]));
        }
    }
}
