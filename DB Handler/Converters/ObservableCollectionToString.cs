﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace DB_Handler.Converters
{
    internal class ObservableCollectionToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ObservableCollection<string>)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var line in value as ObservableCollection<string>)
                    sb.AppendLine(line);

                return sb.ToString();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
