﻿using DB_Handler.Models;
using DB_Handler.Views;
using Extra.Utilities;
using System;
using System.Globalization;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace DB_Handler
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        internal static DBHSettings DBHSettings = new DBHSettings();
        internal static SettingsFile SettingsFile = new SettingsFile();

        #region Exception handlers

        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            new ExceptionViewer("An unrecoverable error has occurred", e.Exception, Current.MainWindow).ShowDialog();

            Current.Shutdown(1);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ExceptionViewer("An unrecoverable error has occurred", (Exception)e.ExceptionObject, Current.MainWindow).ShowDialog();

            Current.Shutdown(1);
        }

        #endregion Exception handlers

        #region Application settings

        internal static void WriteDBHSettings(MainWindow mw = null)
        {
            if (mw != null)
            {
                SettingsFile.WriteSetting("Window", "Height", mw.Height);
                SettingsFile.WriteSetting("Window", "Left", mw.Left);
                SettingsFile.WriteSetting("Window", "Maximized", mw.WindowState == WindowState.Maximized ? true : false);
                SettingsFile.WriteSetting("Window", "Top", mw.Top);
                SettingsFile.WriteSetting("Window", "Width", mw.Width);
            }

            SettingsFile.WriteSetting("General", "Language", DBHSettings.SelectedLanguage);

            SettingsFile.WriteSetting("Folder", "Downloads", DBHSettings.DownloadsPath);
            SettingsFile.WriteSetting("Folder", "RAPs", DBHSettings.RAPsPath);

            SettingsFile.WriteSetting("Database", "Database", DBHSettings.DatabasePath);
            SettingsFile.WriteSetting("Database", "Latest Entries", DBHSettings.LatestEntries);
            SettingsFile.WriteSetting("Database", "LID only", DBHSettings.LIDOnly);
            SettingsFile.WriteSetting("Database", "Search PS3", DBHSettings.SearchPS3);
            SettingsFile.WriteSetting("Database", "Search PS4", DBHSettings.SearchPS4);
            SettingsFile.WriteSetting("Database", "Search PSP", DBHSettings.SearchPSP);
            SettingsFile.WriteSetting("Database", "Search PSV", DBHSettings.SearchPSV);
            SettingsFile.WriteSetting("Database", "Show CID Region", DBHSettings.ShowCIDRegion);
        }

        private static void LoadLanguage()
        {
            int langCodePosition = DBHSettings.SelectedLanguage.IndexOf('(');

            if (langCodePosition == -1)
                return;
            else
                langCodePosition++;

            string language = DBHSettings.SelectedLanguage.Substring(langCodePosition, 2);

            try
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(language);
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(language);
            }
            catch (CultureNotFoundException) { return; }
        }

        private static void ReadDBHSettings()
        {
            DBHSettings.SelectedLanguage = SettingsFile.ReadSetting("General", "Language", DBHSettings.AVAILABLE_LANGUAGES[0]);
            DBHSettings.DeveloperMode = SettingsFile.ReadSetting("General", "Developer Mode", false);

            DBHSettings.DatabasePath = SettingsFile.ReadSetting("Database", "Database", "Sony.db");
            DBHSettings.LatestEntries = SettingsFile.ReadSetting("Database", "Latest Entries", (byte)20);
            DBHSettings.LIDOnly = SettingsFile.ReadSetting("Database", "LID only", false);
            DBHSettings.SearchPS3 = SettingsFile.ReadSetting("Database", "Search PS3", true);
            DBHSettings.SearchPS4 = SettingsFile.ReadSetting("Database", "Search PS4", true);
            DBHSettings.SearchPSP = SettingsFile.ReadSetting("Database", "Search PSP", true);
            DBHSettings.SearchPSV = SettingsFile.ReadSetting("Database", "Search PSV", true);
            DBHSettings.ShowCIDRegion = SettingsFile.ReadSetting("Database", "Show CID Region", true);

            DBHSettings.DownloadsPath = SettingsFile.ReadSetting("Folder", "Downloads", "Downloads");
            DBHSettings.RAPsPath = SettingsFile.ReadSetting("Folder", "RAPs", "RAPs");
        }

        #endregion Application settings

        #region Entry point

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            ReadDBHSettings();

            LoadLanguage();

            MainWindow mainWindow = new MainWindow();
            Current.MainWindow = mainWindow;

            mainWindow.Show();
        }

        #endregion Entry point
    }
}
