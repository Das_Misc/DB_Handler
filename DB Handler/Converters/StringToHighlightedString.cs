﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DB_Handler.Converters
{
    internal class StringToHighlightedString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
                return value.ToString().Replace(" ", "");

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
