﻿using DB_Handler.Views;
using Extra.Utilities;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Extra.Controls
{
    /// <summary>
    /// Interaction logic for MyWindow.xaml
    /// </summary>
    public partial class MyMergeWindow : Window
    {
        private ListViewDragDropManager<string> dragManager;

        public MyMergeWindow()
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            Style = FindResource("MyMergeWindowStyle") as Style;

            OutputFile = new ObservableCollection<string>();
            OutputFile.Add(string.Empty);

            MergeProgress = new ObservableCollection<double>();
            MergeProgress.Add(0);

            SplitFiles = new ObservableCollection<string>();

            InitializeComponent();
        }

        public ObservableCollection<double> MergeProgress { get; set; }
        public ObservableCollection<string> OutputFile { get; set; }
        public ObservableCollection<string> SplitFiles { get; set; }

        #region CanExecute

        private void Merge_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            var listView = VisualTreeHelpers.FindChild<ListView>(this);

            e.CanExecute = (listView != null) && listView.HasItems && !string.IsNullOrWhiteSpace(OutputFile[0]);
        }

        private void SaveFile_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            var listView = VisualTreeHelpers.FindChild<ListView>(this);

            e.CanExecute = (listView != null) && listView.HasItems;
        }

        #endregion CanExecute

        #region Executed

        private void AddFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.Multiselect = true;
            ofd.FileName = DB_Handler.Properties.Resources.MergeWindowSelectionMessage;

            if (ofd.ShowDialog().Value)
            {
                string[] splitFiles = null;

                if (File.Exists(ofd.FileName))
                    splitFiles = ofd.FileNames;
                else if (ofd.FileName.EndsWith(DB_Handler.Properties.Resources.MergeWindowSelectionMessage))
                    splitFiles = Directory.GetFiles(Path.GetDirectoryName(ofd.FileName), "*", SearchOption.AllDirectories);
                else
                    return;

                foreach (var splitFile in splitFiles)
                    if (!SplitFiles.Contains(splitFile))
                        SplitFiles.Add(splitFile);
            }
        }

        private async void Merge_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var listView = VisualTreeHelpers.FindChild<ListView>(this);

            await Task.Run(() =>
            {
                long currentProgress = 0;
                long total = 0;

                for (int i = 0; i < listView.Items.Count; i++)
                {
                    var fi = new FileInfo(listView.Items.GetItemAt(i).ToString());

                    total += fi.Length;
                }

                IProgress<double> progress = new Progress<double>(UpdateProgress);

                MainWindow.AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format(DB_Handler.Properties.Resources.MergingFiles, OutputFile[0])));

                File.Copy(listView.Items.GetItemAt(0).ToString(), OutputFile[0], true);

                currentProgress = new FileInfo(listView.Items.GetItemAt(0).ToString()).Length;

                progress.Report((currentProgress * 100) / total);

                using (var fs = File.Open(OutputFile[0], FileMode.Append, FileAccess.Write, FileShare.Read))
                    for (int i = 1; i < listView.Items.Count; i++)
                        using (var inFS = File.Open(listView.Items.GetItemAt(i).ToString(), FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            inFS.CopyTo(fs);

                            currentProgress += inFS.Length;

                            progress.Report((currentProgress * 100) / total);
                        }

                MainWindow.AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format(DB_Handler.Properties.Resources.MergedFiles, OutputFile[0])));
            });
        }

        private void SaveFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.AddExtension = true;
            sfd.DefaultExt = "pkg";

            if (sfd.ShowDialog().Value)
                OutputFile[0] = (sfd.FileName);
        }

        private void SubtractFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var listView = VisualTreeHelpers.FindChild<ListView>(this);

            if (listView.SelectedValue != null)
                SplitFiles.Remove(listView.SelectedValue.ToString());
        }

        private void UpdateProgress(double obj)
        {
            MergeProgress[0] = obj;
        }

        #endregion Executed

        #region Window States

        private void MergeWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var listView = VisualTreeHelpers.FindChild<ListView>(this);

            dragManager = new ListViewDragDropManager<string>(listView);
            listView.ItemContainerStyle = FindResource("ItemContStyle") as Style;
        }

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        #endregion Window States
    }
}
