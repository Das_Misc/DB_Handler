﻿using Extra.Utilities;
using System;
using System.Numerics;
using System.Security.Cryptography;

namespace RAP_Validator
{
    internal class CMAC
    {
        private int hashLen = 0x10;
        private byte[] K1;
        private byte[] K2;
        private byte[] key;
        private byte[] nonProcessed;
        private byte[] previous;

        private void CalculateSubKey(byte[] key, byte[] K1, byte[] K2)
        {
            byte[] @in = new byte[0x10];

            byte[] @out = CryptographicEngines.Encrypt(@in, key, null, CipherMode.ECB, PaddingMode.None);

            BigInteger integer = new BigInteger(ReverseByteWithSize(@out));

            if ((@out[0] & 0x80) != 0)
                integer = (integer << 1) ^ new BigInteger(0x87);
            else
                integer = integer << 1;

            byte[] src = ReverseByteWithSize(integer.ToByteArray());

            if (src.Length >= 0x10)
                Array.Copy(src, src.Length - 0x10, K1, 0, 0x10);
            else
            {
                Array.Copy(@in, K1, @in.Length);

                Array.Copy(src, 0, K1, (long)(0x10 - src.Length), src.Length);
            }

            integer = new BigInteger(ReverseByteWithSize(K1));

            if ((K1[0] & 0x80) != 0)
                integer = (integer << 1) ^ new BigInteger(0x87);
            else
                integer = integer << 1;

            src = ReverseByteWithSize(integer.ToByteArray());

            if (src.Length >= 0x10)
                Array.Copy(src, src.Length - 0x10, K2, 0, 0x10);
            else
            {
                Array.Copy(@in, K2, @in.Length);

                Array.Copy(src, 0, K2, (long)(0x10 - src.Length), src.Length);
            }
        }

        internal bool DoFinal(byte[] expectedhash, int hashOffset)
        {
            byte[] dest = new byte[0x10];

            Array.Copy(nonProcessed, dest, nonProcessed.Length);

            if (nonProcessed.Length == 0x10)
                dest = CryptographicEngines.XOR(dest, K1);
            else
            {
                dest[nonProcessed.Length] = 0x80;

                dest = CryptographicEngines.XOR(dest, K2);
            }

            dest = CryptographicEngines.XOR(dest, previous);

            byte[] @out = CryptographicEngines.Encrypt(dest, key, null, CipherMode.ECB, PaddingMode.None);

            bool result = CryptographicEngines.CompareBytes(expectedhash, hashOffset, @out, 0, hashLen);

            return result;
        }

        internal void DoInit(byte[] key)
        {
            this.key = key;

            K1 = new byte[0x10];
            K2 = new byte[0x10];

            CalculateSubKey(key, K1, K2);

            nonProcessed = null;

            previous = new byte[0x10];
        }

        internal void DoUpdate(byte[] @in, int inOffset, int length)
        {
            byte[] buffer;

            if (nonProcessed != null)
            {
                int num = length + nonProcessed.Length;

                buffer = new byte[num];

                Array.Copy(nonProcessed, buffer, nonProcessed.Length);

                Array.Copy(@in, inOffset, buffer, (long)nonProcessed.Length, length);
            }
            else
            {
                buffer = new byte[length];

                Array.Copy(@in, inOffset, buffer, 0, length);
            }

            int srcPos = 0;

            byte[] dest = new byte[0x10];

            while (srcPos < (buffer.Length - 0x10))
            {
                Array.Copy(buffer, srcPos, dest, 0, dest.Length);

                dest = CryptographicEngines.XOR(dest, previous);

                previous = CryptographicEngines.Encrypt(dest, key, null, CipherMode.ECB, PaddingMode.None);

                srcPos += 0x10;
            }

            nonProcessed = new byte[buffer.Length - srcPos];

            Array.Copy(buffer, srcPos, nonProcessed, 0, nonProcessed.Length);
        }

        private static byte[] ReverseByteWithSize(byte[] value)
        {
            int num;

            byte[] buffer;

            if ((value.Length < 0x10) && ((value[value.Length - 1] & 0x80) != 0))
            {
                buffer = new byte[0x10];

                for (num = 0; num < value.Length; num++)
                    buffer[(buffer.Length - 1) - num] = value[num];

                for (num = value.Length; num < 0x10; num++)
                    buffer[15 - num] = 0xff;

                return buffer;
            }

            buffer = new byte[value.Length];

            for (num = 0; num < buffer.Length; num++)
                buffer[(buffer.Length - 1) - num] = value[num];

            return buffer;
        }
    }
}
