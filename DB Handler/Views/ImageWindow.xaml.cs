﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace DB_Handler.Views
{
    /// <summary>
    /// Interaction logic for ImageWindow.xaml
    /// </summary>
    public partial class ImageWindow : Window
    {
        public ImageWindow(ImageSource imageSource)
        {
            Image = new ObservableCollection<ImageSource>();

            Image.Add(imageSource);

            InitializeComponent();

            Width = imageSource.Width;
            Height = imageSource.Height;
        }

        public ObservableCollection<ImageSource> Image { get; set; }

        private void ImageWindow_Deactivated(object sender, EventArgs e)
        {
            Close();
        }

        #region Events

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                Deactivated -= ImageWindow_Deactivated;

                Close();
            }
        }

        #endregion Events
    }
}
