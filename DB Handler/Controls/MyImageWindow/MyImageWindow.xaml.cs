﻿using DB_Handler.Converters;
using DB_Handler.Functions;
using DB_Handler.Models;
using DB_Handler.Views;
using Extra.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Extra.Controls
{
    /// <summary>
    /// Interaction logic for MyWindow.xaml
    /// </summary>
    public partial class MyImageWindow : Window
    {
        public ObservableCollection<ImageSource> Images { get; set; }

        public ObservableCollection<string> Information { get; set; }

        public MyImageWindow(DatabaseEntry dbEntry)
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            Style = FindResource("MyImageWindowStyle") as Style;

            DataContext = this;

            Images = new ObservableCollection<ImageSource>();

            Information = new ObservableCollection<string>();
            Information.Add(dbEntry.Title);
            Information.Add(string.Empty);

            InitializeComponent();

            DisplayInfo(dbEntry.Link);
        }

        #region Load methods

        private void DisplayImages(List<JToken> imageTokens)
        {
            for (int i = 0; i < imageTokens.Count; i++)
            {
                string imageURL = imageTokens[i].ToObject(typeof(string)).ToString();

                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(imageURL, UriKind.Absolute);
                bitmap.EndInit();

                Images.Add(bitmap);
            }
        }

        private async void DisplayInfo(string link)
        {
            await Task.Run(() =>
            {
                LoadData(link);
            });
        }

        private void LoadData(string link)
        {
            var pkgInfo = PSWebStore.GetPKGInfo(link, true);

            var storeLinks = PSWebStore.GetStoreLinks(pkgInfo.CID);

            bool found = true;

            try
            {
                using (var request = new WebClient())
                {
                    request.Headers.Add("User-Agent", string.Empty);

                    using (var stream = request.OpenRead(storeLinks.jsonLink))
                    using (var reader = new StreamReader(stream))
                    {
                        var json = JObject.Parse(reader.ReadToEnd());

                        LoadDescription(json);

                        LoadImages(json);
                    }
                }
            }
            catch (WebException) { found = false; }

            LoadInformation(pkgInfo, found);
        }

        private void LoadDescription(JObject json)
        {
            string rawDescription = json["long_desc"].ToString().Replace("<br>", "\n");
            string strippedDescription = MiscUtils.StripHTML(rawDescription);
            string description = HttpUtility.HtmlDecode(strippedDescription) + Environment.NewLine + Environment.NewLine;

            Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
            {
                Information[0] += string.Format(" - {0}", json["name"].ToString());

                Information[1] = description;
            });
        }

        private void LoadImages(JObject json)
        {
            var coverImagesTokens = new List<JToken>(json["images"].Children().Values("url"));

            coverImagesTokens.RemoveRange(1, coverImagesTokens.Count - 1);

            Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
            {
                DisplayImages(coverImagesTokens);
            });

            var imagesTokens = new List<JToken>(json["promomedia"].Children().Values("thumbnails").Children());

            Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
            {
                DisplayImages(imagesTokens);
            });
        }

        private void LoadInformation(PKGInfo pkgInfo, bool found)
        {
            var sizeToString = new SizeToString();

            var information = new StringBuilder();

            if (!found)
            {
                information.AppendLine(DB_Handler.Properties.Resources.WebStoreNotFound);
                information.AppendLine();
                information.AppendLine();
            }

            information.AppendLine(string.Format("CID: " + pkgInfo.CID));
            information.AppendLine(string.Format(new CultureInfo("es-es"), "{0}: {1} ({2:#,###} B)", DB_Handler.Properties.Resources.Size, sizeToString.Convert(pkgInfo.Size, null, null, null), pkgInfo.Size));

            if (pkgInfo.IsPS3orPSPPKG)
            {
                information.AppendLine(string.Format("{0}: {1}", DB_Handler.Properties.Resources.FilesFolders, pkgInfo.FilesAndFolders));
                information.AppendLine(string.Format("{0}: {1}", DB_Handler.Properties.Resources.LicenseType, pkgInfo.LicenseType));

                if (!string.IsNullOrEmpty(pkgInfo.InstallDirectory))
                    information.AppendLine(string.Format("{0}: {1}", DB_Handler.Properties.Resources.InstallDirectory, pkgInfo.InstallDirectory));

                if (!string.IsNullOrEmpty(pkgInfo.AppVersion))
                    information.AppendLine("APP_VERSION: " + pkgInfo.AppVersion);

                if (!string.IsNullOrEmpty(pkgInfo.PackageVersion))
                    information.AppendLine("PKG_VERSION: " + pkgInfo.PackageVersion);

                if (!string.IsNullOrEmpty(pkgInfo.PS3SystemVersion))
                    information.AppendLine("PS3_SYSTEM_VERSION: " + pkgInfo.PS3SystemVersion);
            }

            Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
            {
                Information[1] += information.ToString();
            });
        }

        #endregion Load methods

        #region Executed

        private void OpenImageWindow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var imageButton = (e.OriginalSource as Button);
            var image = (imageButton.Content as Image);

            var imageWindow = new ImageWindow(image.Source);
            imageWindow.Closing += (theSender, args) => { imageWindow.Owner = null; };

            // Get absolute location on screen of upper left corner of image
            var imageLocationFromScreen = image.PointToScreen(new Point(0, 0));

            // Transform screen point to WPF device independent point
            var target = PresentationSource.FromVisual(this).CompositionTarget.TransformFromDevice.Transform(imageLocationFromScreen);

            imageWindow.Top = target.Y - (image.Height / 2);
            imageWindow.Left = target.X - (image.Width / 2);

            imageWindow.Show();
        }

        #endregion Executed

        #region Window States

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        #endregion Window States
    }
}
