﻿using System.ComponentModel;

namespace DB_Handler.Models
{
    public class DatabaseEntry : INotifyPropertyChanged
    {
        #region Fields

        private bool isSelected;
        private long size;
        private string title, titleID, cid, rap, link, rowid;

        #endregion Fields

        #region Properties

        public string CID
        {
            get { return cid; }
            set
            {
                if (cid == value) return;
                cid = value;
                NotifyPropertyChanged("CID");
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }

        public string Link
        {
            get { return link; }
            set
            {
                if (link == value) return;
                link = value;
                NotifyPropertyChanged("Link");
            }
        }

        public string RAP
        {
            get { return rap; }
            set
            {
                if (rap == value) return;
                rap = value;
                NotifyPropertyChanged("RAP");
            }
        }

        public string RowID
        {
            get { return rowid; }
            set
            {
                if (rowid == value) return;
                rowid = value;
                NotifyPropertyChanged("RowID");
            }
        }

        public long Size
        {
            get { return size; }
            set
            {
                if (size == value) return;
                size = value;
                NotifyPropertyChanged("Size");
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                if (title == value) return;
                title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public string TitleID
        {
            get { return titleID; }
            set
            {
                if (titleID == value) return;
                titleID = value;
                NotifyPropertyChanged("TitleID");
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged Implementation
    }
}
