﻿using System.ComponentModel;

namespace DB_Handler.Models
{
    public class TitleDatabaseEntry : INotifyPropertyChanged
    {
        #region Fields

        private string title, titleID;

        #endregion Fields

        #region Properties

        public string Title
        {
            get { return title; }
            set
            {
                if (title == value) return;
                title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public string TitleID
        {
            get { return titleID; }
            set
            {
                if (titleID == value) return;
                titleID = value;
                NotifyPropertyChanged("TitleID");
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged Implementation
    }
}
