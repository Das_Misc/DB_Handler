﻿using DB_Handler.Views;
using Extra.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Extra.Functions
{
    internal class Downloader
    {
        #region Fields

        private string downloadDirectory;
        private Queue<string> downloadUrls;
        private string filename;

        private WebClient webClient;

        internal delegate void StatusUpdateHandler(object sender, DownloadStatus e);

        internal event StatusUpdateHandler OnUpdateStatus;

        #endregion Fields

        #region Constructor

        internal Downloader(string downloadDirectory)
        {
            this.downloadDirectory = downloadDirectory;

            if (!Directory.Exists(downloadDirectory))
                Directory.CreateDirectory(downloadDirectory);

            downloadUrls = new Queue<string>();

            webClient = new WebClient();

            webClient.DownloadProgressChanged += WebClient_DownloadProgressChanged;
            webClient.DownloadFileCompleted += WebClient_DownloadFileCompleted;
        }

        #endregion Constructor

        #region Internal methods

        internal void CancelDownload()
        {
            webClient.CancelAsync();
        }

        internal async void DownloadFiles(IEnumerable<string> urls)
        {
            await Task.Run(() =>
            {
                foreach (string url in urls)
                    downloadUrls.Enqueue(url);

                DownloadNext();
            });
        }

        #endregion Internal methods

        #region Private methods

        private void DownloadNext()
        {
            if (downloadUrls.Any())
            {
                var url = new Uri(downloadUrls.Dequeue());

                filename = Path.Combine(downloadDirectory, Path.GetFileName(url.LocalPath));

                webClient.DownloadFileAsync(url, filename);

                MainWindow.AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format(DB_Handler.Properties.Resources.DownloadingFile, Path.GetFileName(url.LocalPath))));
            }
        }

        private void UpdateStatus(string status, string filename)
        {
            if (OnUpdateStatus == null)
                return;

            var args = new DownloadStatus(status, filename);

            OnUpdateStatus(this, args);
        }

        private void UpdateStatus(long bytesReceived, long totalBytesToReceive, string status, string filename)
        {
            // Make sure someone is listening to event
            if (OnUpdateStatus == null)
                return;

            var args = new DownloadStatus(bytesReceived, totalBytesToReceive, status, filename);

            OnUpdateStatus(this, args);
        }

        private void WebClient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null || e.Cancelled)
            {
                File.Delete(filename);

                UpdateStatus(DB_Handler.Properties.Resources.Canceled, filename);

                MainWindow.AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format(DB_Handler.Properties.Resources.CanceledFile, Path.GetFileName(filename))));
            }
            else
            {
                UpdateStatus(DB_Handler.Properties.Resources.Downloaded, filename);

                MainWindow.AppLog.Add(string.Format("[{0}] {1}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), string.Format(DB_Handler.Properties.Resources.DownloadedFile, Path.GetFileName(filename))));
            }

            DownloadNext();
        }

        private void WebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            UpdateStatus(e.BytesReceived, e.TotalBytesToReceive, DB_Handler.Properties.Resources.Downloading, filename);
        }

        #endregion Private methods
    }
}
