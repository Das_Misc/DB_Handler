﻿using System.ComponentModel;

namespace DB_Handler.Models
{
    internal class DBHSettings : INotifyPropertyChanged
    {
        #region Fields

        private static readonly string[] availableLanguages = { "English (en)", "Spanish (es)" };
        private string databasePath;
        private string downloadsPath, rapsPath;
        private byte latestEntries;
        private bool searchPS3, searchPS4, searchPSP, searchPSV, lidOnly, showCIDRegion, developerMode;
        private string selectedLanguage;

        #endregion Fields

        #region Properties

        public static string[] AVAILABLE_LANGUAGES { get { return availableLanguages; } }

        public string DatabasePath
        {
            get { return databasePath; }
            set
            {
                if (databasePath == value) return;
                databasePath = value;
                NotifyPropertyChanged("DatabasePath");
            }
        }

        public bool DeveloperMode
        {
            get { return developerMode; }
            set
            {
                if (developerMode == value) return;
                developerMode = value;
                NotifyPropertyChanged("DeveloperMode");
            }
        }

        public string DownloadsPath
        {
            get { return downloadsPath; }
            set
            {
                if (downloadsPath == value) return;
                downloadsPath = value;
                NotifyPropertyChanged("DownloadsPath");
            }
        }

        public byte LatestEntries
        {
            get { return latestEntries; }
            set
            {
                if (latestEntries == value) return;
                latestEntries = value;
                NotifyPropertyChanged("LatestEntries");
            }
        }

        public bool LIDOnly
        {
            get { return lidOnly; }
            set
            {
                if (lidOnly == value) return;
                lidOnly = value;
                NotifyPropertyChanged("LIDOnly");
            }
        }

        public string RAPsPath
        {
            get { return rapsPath; }
            set
            {
                if (rapsPath == value) return;
                rapsPath = value;
                NotifyPropertyChanged("RAPsPath");
            }
        }

        public bool SearchPS3
        {
            get { return searchPS3; }
            set
            {
                if (searchPS3 == value) return;
                searchPS3 = value;
                NotifyPropertyChanged("SearchPS3");
            }
        }

        public bool SearchPS4
        {
            get { return searchPS4; }
            set
            {
                if (searchPS4 == value) return;
                searchPS4 = value;
                NotifyPropertyChanged("SearchPS4");
            }
        }

        public bool SearchPSP
        {
            get { return searchPSP; }
            set
            {
                if (searchPSP == value) return;
                searchPSP = value;
                NotifyPropertyChanged("SearchPSP");
            }
        }

        public bool SearchPSV
        {
            get { return searchPSV; }
            set
            {
                if (searchPSV == value) return;
                searchPSV = value;
                NotifyPropertyChanged("SearchPSV");
            }
        }

        public string SelectedLanguage
        {
            get { return selectedLanguage; }
            set
            {
                if (selectedLanguage == value) return;
                selectedLanguage = value;
                NotifyPropertyChanged("SelectedLanguage");
            }
        }

        public bool ShowCIDRegion
        {
            get { return showCIDRegion; }
            set
            {
                if (showCIDRegion == value) return;
                showCIDRegion = value;
                NotifyPropertyChanged("ShowCIDRegion");
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged Implementation
    }
}
