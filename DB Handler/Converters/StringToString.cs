﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DB_Handler.Converters
{
    internal class StringToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                if (value.ToString().Contains(Properties.Resources.Downloading))
                    return Properties.Resources.Cancel;
                else
                    return Properties.Resources.StartDownload;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
