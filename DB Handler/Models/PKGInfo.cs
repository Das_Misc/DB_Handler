﻿namespace DB_Handler.Models
{
    internal class PKGInfo
    {
        #region Fields

        private string cid, title, titleID, ps3SystemVersion, appVersion, packageVersion, installDirectory;
        private int filesAndFolders;
        private bool isPS3orPSPPKG;
        private byte licenseType;
        private long size;

        #endregion Fields

        #region Properties

        public string AppVersion
        {
            get { return appVersion; }
            set
            {
                if (appVersion == value) return;
                appVersion = value;
            }
        }

        public string CID
        {
            get { return cid; }
            set
            {
                if (cid == value) return;
                cid = value;
            }
        }

        public int FilesAndFolders
        {
            get { return filesAndFolders; }
            set
            {
                if (filesAndFolders == value) return;
                filesAndFolders = value;
            }
        }

        public string InstallDirectory
        {
            get { return installDirectory; }
            set
            {
                if (installDirectory == value) return;
                installDirectory = value;
            }
        }

        public bool IsPS3orPSPPKG
        {
            get { return isPS3orPSPPKG; }
            set
            {
                if (isPS3orPSPPKG == value) return;
                isPS3orPSPPKG = value;
            }
        }

        public byte LicenseType
        {
            get { return licenseType; }
            set
            {
                if (licenseType == value) return;
                licenseType = value;
            }
        }

        public string PackageVersion
        {
            get { return packageVersion; }
            set
            {
                if (packageVersion == value) return;
                packageVersion = value;
            }
        }

        public string PS3SystemVersion
        {
            get { return ps3SystemVersion; }
            set
            {
                if (ps3SystemVersion == value) return;
                ps3SystemVersion = value;
            }
        }

        public long Size
        {
            get { return size; }
            set
            {
                if (size == value) return;
                size = value;
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                if (title == value) return;
                title = value;
            }
        }

        public string TitleID
        {
            get { return titleID; }
            set
            {
                if (titleID == value) return;
                titleID = value;
            }
        }

        #endregion Properties
    }
}
