﻿namespace DB_Handler.Models
{
    internal class P3TInfo
    {
        #region Properties

        public string Cid { get; set; }

        public long Size { get; set; }

        public string Title { get; set; }

        #endregion Properties
    }
}
