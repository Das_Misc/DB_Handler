﻿using DB_Handler;
using DB_Handler.Functions;
using DB_Handler.Models;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Extra.Controls
{
    /// <summary>
    /// Interaction logic for MyWindow.xaml
    /// </summary>
    public partial class MyTitlesWindow : Window
    {
        public static ObservableCollection<TitleDatabaseEntry> TitleDatabaseEntries { get; set; }

        public MyTitlesWindow()
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            Style = FindResource("MyTitlesWindowStyle") as Style;

            TitleDatabaseEntries = new ObservableCollection<TitleDatabaseEntry>();

            DataContext = App.DBHSettings;

            InitializeComponent();

            if (App.DBHSettings.LIDOnly)
                DatabaseFunctions.SearchTitleDb(this, null, true);
            else
                DatabaseFunctions.SearchTitleDb(this);
        }

        #region Events

        private void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit && e.EditingElement.IsFocused)
            {
                var dg = sender as DataGrid;

                var dbEntry = dg.CurrentItem as TitleDatabaseEntry;

                if (dbEntry == null)
                {
                    dg.CancelEdit();

                    return;
                }

                DatabaseFunctions.UpdateTitle((e.EditingElement as TextBox).Text, dbEntry.TitleID);
            }
            else if (e.EditAction == DataGridEditAction.Commit && !e.EditingElement.IsFocused)
                (sender as DataGrid).CancelEdit();
        }

        private void textBox_Search(object sender, RoutedEventArgs e)
        {
            TitleDatabaseEntries.Clear();

            DatabaseFunctions.SearchTitleDb(this, (sender as SearchTextBox).Text, App.DBHSettings.LIDOnly);
        }

        #endregion Events

        #region Executed

        private void DataGridScrollToBottom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dg = sender as DataGrid;

            dg.SelectedItem = dg.Items.GetItemAt(dg.Items.Count - 1);

            dg.ScrollIntoView(dg.SelectedItem, dg.CurrentColumn);
        }

        private void LIDonlyToggle_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            TitleDatabaseEntries.Clear();

            if (App.DBHSettings.LIDOnly)
                DatabaseFunctions.SearchTitleDb(this, searchTextBox.Text, true);
            else
                DatabaseFunctions.SearchTitleDb(this, searchTextBox.Text);
        }

        #endregion Executed

        #region Window States

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        #endregion Window States
    }
}
