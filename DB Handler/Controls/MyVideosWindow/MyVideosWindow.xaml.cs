﻿using DB_Handler.Functions;
using DB_Handler.Models;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Extra.Controls
{
    /// <summary>
    /// Interaction logic for MyWindow.xaml
    /// </summary>
    public partial class MyVideosWindow : Window
    {
        public MyVideosWindow()
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            Style = FindResource("MyVideosWindowStyle") as Style;

            VideoDatabaseEntries = new ObservableCollection<VideoDatabaseEntry>();

            DataContext = VideoDatabaseEntries;

            InitializeComponent();

            DatabaseFunctions.SearchVideoDB(this);

            VideoDatabaseEntries.CollectionChanged += VideoDatabaseEntries_CollectionChanged;
        }

        public static ObservableCollection<VideoDatabaseEntry> VideoDatabaseEntries { get; set; }

        #region Events

        private void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit && e.EditingElement.IsFocused)
            {
                var dg = (sender as DataGrid);

                var vdbEntry = dg.CurrentItem as VideoDatabaseEntry;

                if (vdbEntry == null)
                {
                    dg.CancelEdit();

                    return;
                }

                string newTitle = (e.EditingElement as TextBox).Text;

                DatabaseFunctions.UpdateVideoTitle(newTitle, vdbEntry.Link);
            }
            else if (e.EditAction == DataGridEditAction.Commit && !e.EditingElement.IsFocused)
                (sender as DataGrid).CancelEdit();
        }

        private void textBox_Search(object sender, RoutedEventArgs e)
        {
            VideoDatabaseEntries.Clear();

            DatabaseFunctions.SearchVideoDB(this, (sender as SearchTextBox).Text);
        }

        private void VideoDatabaseEntries_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                var dbEntry = e.OldItems[0] as VideoDatabaseEntry;

                DatabaseFunctions.DeleteVideoEntry(dbEntry.Link);
            }
        }

        #endregion Events

        #region Window States

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        #endregion Window States
    }
}
