﻿using DB_Handler;
using DB_Handler.Functions;
using DB_Handler.Models;
using DB_Handler.Views;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Extra.Controls
{
    /// <summary>
    /// Interaction logic for MyWindow.xaml
    /// </summary>
    public partial class MySettingsWindow : Window
    {
        private bool changesMade = false;
        private MainWindow mw;
        private bool restartApplication = false;

        private DBHSettings undoChanges;

        public MySettingsWindow(MainWindow mw)
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            Style = FindResource("MySettingsWindowStyle") as Style;

            undoChanges = App.DBHSettings.Copy();

            DataContext = App.DBHSettings;

            this.mw = mw;

            InitializeComponent();
        }

        #region CanExecute

        private void OK_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (NumericBox.Value != undoChanges.LatestEntries)
                changesMade = true;

            e.CanExecute = changesMade;
        }

        #endregion CanExecute

        #region Events

        private void Language_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem.ToString() != undoChanges.SelectedLanguage)
            {
                changesMade = true;
                restartApplication = true;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Close_Executed(null, null);
        }

        #endregion Events

        #region Executed

        private void BrowseDatabase_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckFileExists = false;
            ofd.DefaultExt = "db";
            ofd.Filter = DB_Handler.Properties.Resources.SettingsDBFilter;
            ofd.Multiselect = false;
            ofd.Title = DB_Handler.Properties.Resources.BrowseDialogSelectDBTitle;

            if (ofd.ShowDialog().HasValue && ofd.FileName != App.DBHSettings.DatabasePath && !string.IsNullOrEmpty(ofd.FileName))
            {
                App.DBHSettings.DatabasePath = ofd.FileName;

                changesMade = true;
            }
        }

        private void BrowseDownloads_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.FileName = DB_Handler.Properties.Resources.BrowseDialogSelectFolder;
            ofd.Title = DB_Handler.Properties.Resources.BrowseDialogSelectFolderTitle;

            if (ofd.ShowDialog().HasValue && !File.Exists(ofd.FileName))
            {
                App.DBHSettings.DownloadsPath = ofd.FileName;

                changesMade = true;
            }
        }

        private void BrowseRAPs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.FileName = DB_Handler.Properties.Resources.BrowseDialogSelectFolder;
            ofd.Title = DB_Handler.Properties.Resources.BrowseDialogSelectFolderTitle;

            if (ofd.ShowDialog().HasValue && !File.Exists(ofd.FileName))
            {
                App.DBHSettings.RAPsPath = ofd.FileName;

                changesMade = true;
            }
        }

        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DBHSettings = undoChanges.Copy();

            Close();
        }

        private void OK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.WriteDBHSettings();

            undoChanges = App.DBHSettings.Copy();

            if (!File.Exists(App.DBHSettings.DatabasePath))
                DatabaseFunctions.CreateDatabase();

            if (restartApplication)
            {
                Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();
            }

            Close();
        }

        private void ShowCIDRegion_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            changesMade = true;

            mw.DatabaseEntries.Clear();

            DatabaseFunctions.SearchMainDb(mw, mw.searchTextBox.Text);
        }

        #endregion Executed

        #region Window States

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        #endregion Window States
    }
}
