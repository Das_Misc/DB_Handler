﻿namespace DB_Handler.Models
{
    public class VideoDatabaseEntry : TitleDatabaseEntry
    {
        #region Fields

        private string link;

        #endregion Fields

        #region Properties

        public string Link
        {
            get { return link; }
            set
            {
                if (link == value) return;
                link = value;
                NotifyPropertyChanged("Link");
            }
        }

        #endregion Properties
    }
}
