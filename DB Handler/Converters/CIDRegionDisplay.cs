﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DB_Handler.Converters
{
    internal class CIDRegionDisplay : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                if (App.DBHSettings.ShowCIDRegion)
                    return (string)value;
                else
                    return (value as string).Substring(7);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
